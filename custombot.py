import discord
from discord.ext import commands
import aiosqlite
import aiohttp
import random


TIPS = [
    "**Need an injection of coins?**\nYou can get an extra 250 coins every 12 hours by voting at\n<https://discordbots.org/bot/606276772295868416/vote>\n",
    "**Want more features?**\nYou can suggest new features to be added with the command `h.suggestfeature <feature>`\n",
    "Visit the website <https://hal-bot.xyz/> if you can't figure out how to run some of the commands\n",
    "**Need support?**\nJoin the official server to get help from the developer\nhttps://discordapp.com/invite/yRc2Yfy\n",
    "**Like the bot?**\nAnything you can donate helps keep this bot alive as long as possible\n<https://donatebot.io/checkout/606204996857495562>\n",
    "More jobs are unlocked as you use the bot more\nAstronaut is unlocked after you have run 5000 commands\nCheck your progress with `h.cmds`\n",
    "**Want me in your own server?**\nUse my invite link from `h.invite` or with the link below\n<https://discordapp.com/api/oauth2/authorize?client_id=606276772295868416&permissions=470117446&scope=bot>\n",
    "**Don't want to see these tips?**\nDisable them with the command `h.tips off`\nThey can be enabled again at any time with `h.tips on`\n"
]


class Bot(commands.Bot):
    def __init__(self, **kwargs):
        super().__init__(
            command_prefix=kwargs.get("command_prefix"),
            case_insensitive=kwargs.get("case_insensitive"),
        )
        self.db_file = "botdata.db"

    async def give_xp(self, **kwargs):
        amount_to_give = int(kwargs.get("amount"))
        user_id = int(kwargs.get("id"))

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                "SELECT exp FROM users WHERE(user_id = ?);",
                [user_id]
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    new_exp = row[0] + amount_to_give
                    await db.execute(
                        "UPDATE users SET exp = ? WHERE user_id = ?",
                        [new_exp, user_id]
                    )
                    await db.commit()
                    return
                else:
                    await db.execute(
                        "INSERT INTO users(user_id, balance, exp, commands_run, job, tips) VALUES(?, 0, ?, 0, NULL, 1);",
                        [user_id, amount_to_give]
                    )
                    await db.commit()
                    return

    async def increment_commands_run(self, id):
        user_id = id

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                "SELECT commands_run FROM users WHERE(user_id = ?);",
                [user_id]
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    amt = row[0]
                    await db.execute(
                        "UPDATE users SET commands_run = ? WHERE user_id = ?",
                        [amt+1, user_id]
                    )
                    await db.commit()
                    return
                else:
                    await db.execute(
                        "INSERT INTO users(user_id, balance, exp, commands_run, job, tips) VALUES(?, 0, 0, 1, NULL, 1);",
                        [user_id]
                    )
                    await db.commit()
                    return

    async def update_commands_stats(self, ctx):
        command_name = ctx.command.name.lower()

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                "SELECT uses FROM cmdstats WHERE(name = ?);", [command_name]
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    await db.execute(
                        "UPDATE cmdstats SET uses = uses + 1 WHERE name = ?",
                        [command_name],
                    )
                    await db.commit()
                    return
                else:
                    await db.execute(
                        "INSERT INTO cmdstats(name, uses) VALUES(?, ?);",
                        [command_name, 1],
                    )
                    await db.commit()
                    return

    async def check_tips_enabled(self, id):
        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute("SELECT tips FROM users WHERE(user_id = ?);", [id]) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return True
                else:
                    return row[0] == 1

    async def send_embed(self, ctx, embed):
        tips_allowed = await self.check_tips_enabled(ctx.author.id)
        if random.randint(1,10) <= 3 and tips_allowed:
            await ctx.send(content=random.choice(TIPS), embed=embed)
        else:
            await ctx.send(embed=embed)
