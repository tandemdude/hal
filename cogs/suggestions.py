import discord
from discord.ext import commands
import datetime
import aiosqlite


class Suggestions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @commands.group(invoke_without_command=True)
    async def suggestions(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```setchannel <channel> - sets the channel for suggestions to be posted in\ndisable - disables suggestions```"
        )

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @suggestions.command()
    async def setchannel(self, ctx, channel: discord.TextChannel = None):
        """Set channel for suggestions to be sent to"""
        if channel is None:
            return await ctx.send(
                "You need to specify a channel for suggestions to appear in."
            )
        else:
            suggestion_channel_id = channel.id

            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        await db.execute(
                            f"UPDATE guilds SET suggestion_id = {suggestion_channel_id} WHERE guild_id = {ctx.guild.id}"
                        )
                        await db.commit()
                    else:
                        await ctx.send(
                            "Please wait, creating a record for you in the database."
                        )
                        await db.execute(
                            f"INSERT INTO guilds(guild_id, welcome_id, role_id, logging_id, suggestion_id, announcement_id) VALUES({ctx.guild.id}, NULL, NULL, NULL, {suggestion_channel_id}, NULL);"
                        )
                        await db.commit()
            return await ctx.send("Suggestions channel registered.")

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @suggestions.command()
    async def disable(self, ctx):
        """Disable suggestions in the guild"""
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send(
                        "No record found. Contact my creator if you believe this to be incorrect."
                    )
                else:
                    await ctx.send("Disabling suggestions...")
                    await db.execute(
                        f"UPDATE guilds SET suggestion_id = NULL WHERE guild_id = {ctx.guild.id}"
                    )
                    await db.commit()
                    return await ctx.send("Suggestions disabled successfully.")

    @commands.guild_only()
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    @commands.command()
    async def suggest(self, ctx, suggestion=None):
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT suggestion_id FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row[0] is not None:
                    chan_id = row[0]
                    channel = ctx.guild.get_channel(chan_id)
                else:
                    return await ctx.send(
                        "This server does not have suggestions enabled."
                    )

        if suggestion is None:
            return await ctx.send("You need to include something to suggest.")
        elif len(suggestion) <= 2048:
            emb = discord.Embed(
                title="New Suggestion:", description=suggestion, colour=0x77DD77
            )
            emb.set_footer(
                icon_url=ctx.author.avatar_url, text=f"Suggested by {str(ctx.author)}"
            )
            msg = await channel.send(embed=emb)
            await msg.add_reaction(":arrow_up:")
            await msg.add_reaction(":arrow_down:")
            return await ctx.send(
                "<:tickYes:315009125694177281> Suggestion sent successfully."
            )
        else:
            return await ctx.send(
                "<:tickNo:315009174163685377> Your suggestion is too long! It must be less than 2048 characters."
            )


def setup(bot):
    bot.add_cog(Suggestions(bot))
