import discord
from discord.ext import commands
import aiosqlite


class Autorole(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db_file = "botdata.db"

    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.group(invoke_without_command=True)
    async def autorole(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```setrole <role> - sets the role to be added on member join\ndisable - disables auto role adding on join```"
        )

    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @autorole.command()
    async def setrole(self, ctx, role: discord.Role = None):
        if role is None:
            return await ctx.send("You need to specify a role.")
        elif role > ctx.guild.me.top_role:
            return await ctx.send(
                "I cannot add a role above mine in the hierachy. Choose a different role below mine, or move my role above the role you want to be added."
            )
        else:
            role_id = role.id

            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        await db.execute(
                            f"UPDATE guilds SET role_id = {role_id} WHERE guild_id = {ctx.guild.id}"
                        )
                        await db.commit()
                    else:
                        await ctx.send(
                            "Please wait, creating a record for you in the database."
                        )
                        await db.execute(
                            f"INSERT INTO guilds(guild_id, welcome_id, role_id, logging_id, suggestion_id, announcement_id) VALUES({ctx.guild.id}, NULL, {role_id}, NULL, NULL, NULL);"
                        )
                        await db.commit()
            return await ctx.send("Role registered.")

    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @autorole.command()
    async def disable(self, ctx):
        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send(
                        "No record found. Contact my creator if you believe this to be incorrect."
                    )
                else:
                    await ctx.send("Disabling welcome messages...")
                    await db.execute(
                        f"UPDATE guilds SET role_id = NULL WHERE guild_id = {ctx.guild.id}"
                    )
                    await db.commit()
                    return await ctx.send("Welcome messages disabled successfully.")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT role_id FROM guilds WHERE(guild_id = {member.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    role = member.guild.get_role(row[0])
                    try:
                        await member.add_roles(role)
                    except (discord.Forbidden, AttributeError):
                        pass


def setup(bot):
    bot.add_cog(Autorole(bot))
