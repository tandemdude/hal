import discord
from discord.ext import commands
import aiosqlite
import datetime


class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db_file = "botdata.db"

    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    @commands.bot_has_permissions(kick_members=True)
    @commands.command()
    async def kick(self, ctx, member: discord.Member = None, *, reason=None):
        """
        Kick a member from the guild
        Format:
            <p>kick @user [reason (optional)]
        Message will be sent to the logging channel if set up
        """
        if member is None:
            return await ctx.send("Please specify a member to kick.")
        elif member.top_role >= ctx.author.top_role:
            return await ctx.send(
                "You cannot kick someone with a role equal to or above yours."
            )
        else:
            try:
                await member.kick(reason=reason)
                await ctx.send(
                    f"<:tickYes:315009125694177281> {str(member)} was kicked."
                )
                async with aiosqlite.connect(self.db_file) as db:
                    async with db.execute(
                        f"SELECT logging_id FROM guilds WHERE(guild_id = {ctx.guild.id});"
                    ) as cursor:
                        row = await cursor.fetchone()

                        if row is not None:
                            logging_channel_id = row[0]
                            try:
                                logging_channel = ctx.guild.get_channel(
                                    logging_channel_id
                                )
                                await logging_channel.send(
                                    embed=discord.Embed(
                                        title=f"{str(member)} was kicked.",
                                        description=f"Kicked requested by {str(ctx.author)}\nReason:\n{reason}",
                                        colour=0xFFB347,
                                        timestamp=datetime.datetime.utcnow(),
                                    )
                                )
                            except (discord.Forbidden, AttributeError):
                                pass
            except (discord.Forbidden, discord.HTTPException):
                await ctx.send(
                    "<:tickNo:315009174163685377> Something went wrong. Maybe the member's role is above mine? If this is not the case please report this to my creator."
                )

    @commands.guild_only()
    @commands.has_permissions(ban_members=True)
    @commands.bot_has_permissions(ban_members=True)
    @commands.command()
    async def ban(self, ctx, member: discord.Member = None, *, reason=None):
        """
        Ban a member from the guild
        Format:
            <p>ban @user [reason (optional)]
        """
        if member is None:
            return await ctx.send("Please specify a member to ban.")
        elif member.top_role >= ctx.author.top_role:
            return await ctx.send(
                "You cannot kick someone with a role equal to or above yours."
            )
        else:
            try:
                await member.ban(reason=reason)
                await ctx.send(
                    f"<:tickYes:315009125694177281> {str(member)} was banned."
                )
            except (discord.Forbidden, discord.HTTPException):
                await ctx.send(
                    "<:tickNo:315009174163685377> Something went wrong. Maybe the member's role is above mine? If this is not the case please report this to my creator."
                )

    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @commands.command()
    async def mute(self, ctx, member: discord.Member = None, *, reason=None):
        """
        Mute a member using a pre-existing `muted` role.
        Member will be muted until manually unmuted.
        __I am not responsible if the role is not set up correctly__
        Format:
            <p>mute @user [reason (optional)]
        """
        if member is None:
            return await ctx.send("Please specify a member to mute.")
        elif member.top_role >= ctx.author.top_role:
            return await ctx.send(
                "You cannot mute someone with a role equal to or above yours."
            )
        else:
            role = discord.utils.get(ctx.guild.roles, name="Muted".casefold())
            if role is None:
                return await ctx.send("No `muted` role exists.")
            try:
                await member.add_roles(role, reason=reason)
                await ctx.send(
                    f"<:tickYes:315009125694177281> {str(member)} was muted."
                )
            except (discord.Forbidden, discord.HTTPException):
                await ctx.send(
                    "<:tickNo:315009174163685377> Something went wrong. Maybe the member's role is above mine? If this is not the case please report this to my creator."
                )

    @commands.guild_only()
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    @commands.command()
    async def unmute(self, ctx, member: discord.Member = None, *, reason=None):
        """
        Unmute a member using a pre-existing `muted` role.
        __I am not responsible if the role is not set up correctly__
        Format:
            <p>unmute @user [reason (optional)]
        """
        if member is None:
            return await ctx.send("Please specify a member to unmute.")
        else:
            role = discord.utils.get(ctx.guild.roles, name="Muted".casefold())
            if role is None:
                return await ctx.send("No `muted` role exists.")
            elif role not in member.roles:
                return await ctx.send("That member is not muted")
            try:
                await member.remove_roles(role, reason=reason)
                await ctx.send(
                    f"<:tickYes:315009125694177281> {str(member)} was unmuted."
                )
            except (discord.Forbidden, discord.HTTPException):
                await ctx.send(
                    "<:tickNo:315009174163685377> Something went wrong. Maybe the member's role is above mine? If this persists please report this to my creator."
                )


def setup(bot):
    bot.add_cog(Moderation(bot))
