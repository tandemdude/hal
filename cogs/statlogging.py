import discord
from discord.ext import commands
import asyncio
import datetime
import logging
import aiosqlite
from cogs.utils.run_in_exec import async_executor
import numpy as np
import matplotlib.pyplot as plt
from io import BytesIO
import re


@async_executor()
def gen_graph(x, y, type):
    plt.plot(x, y, "kx")
    coeffs = np.polyfit(x, y, 3)
    x2 = np.arange(min(x), max(x), 0.01)
    y2 = np.polyval(coeffs, x2)
    plt.plot(x2, y2, label="deg=3")
    plt.xlabel("Days")
    plt.ylabel(type)
    buff = BytesIO()
    plt.savefig(buff, format="png")
    buff.seek(0)
    plt.close()
    return buff


class StatLogging(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.task = self.bot.loop.create_task(self.write_stats())

    async def write_stats(self):
        await self.bot.wait_until_ready()

        while not self.bot.is_closed():
            time = datetime.datetime.utcnow()
            guilds = len(self.bot.guilds)
            users = len([*self.bot.get_all_members()])

            logger.info("Attempting to write stats to database.")
            async with aiosqlite.connect(self.bot.db_file) as db:
                try:
                    await db.execute(
                        f"INSERT INTO statlog(timestamp, guilds, users) VALUES(?, ?, ?);",
                        [time, guilds, users],
                    )
                    await db.commit()
                    logger.info("Stats written successfully")
                except Exception as e:
                    logger.exception(
                        f"Failed to write stats to database\n{type(e).__name__}: {e}"
                    )

            await asyncio.sleep(3600)

    @commands.group(invoke_without_command=True)
    @commands.guild_only()
    async def stats(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```guilds - Show graph of no. guilds over time\nusers - Show graph of no. users over time```"
        )

    @stats.command()
    @commands.guild_only()
    async def guilds(self, ctx):
        today = datetime.datetime.utcnow().date()

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute("SELECT timestamp, guilds FROM statlog;") as cursor:
                rows = await cursor.fetchall()

                dates = []
                guilds = []
                for row in rows:
                    td = (
                        datetime.datetime.strptime(row[0][:10], "%Y-%m-%d").date()
                        - today
                    )
                    dates.append(int(td.days))
                    guilds.append(int(row[1]))
                buff = await gen_graph(dates, guilds, "Guilds")
                await ctx.send(file=discord.File(buff, "out.png"))

    @stats.command()
    @commands.guild_only()
    async def users(self, ctx):
        today = datetime.datetime.utcnow().date()

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute("SELECT timestamp, users FROM statlog;") as cursor:
                rows = await cursor.fetchall()

                dates = []
                users = []
                for row in rows:
                    td = (
                        datetime.datetime.strptime(row[0][:10], "%Y-%m-%d").date()
                        - today
                    )
                    dates.append(int(td.days))
                    users.append(int(row[1]))
                buff = await gen_graph(dates, users, "Users")
                await ctx.send(file=discord.File(buff, "out.png"))


def setup(bot):
    global logger
    logger = logging.getLogger("bot")
    bot.add_cog(StatLogging(bot))
