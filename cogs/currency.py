import discord
from discord.ext import commands
import aiosqlite
import aiohttp
import random
import typing
import asyncio


class Currency(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db_file = "botdata.db"
        self.slots_emojis = [
            "<:scoin:606205354879221781>",
            "<:diamond:606205425569890380>",
            "<:clover:606205332460666890>",
            "<:chip2:606205529282314241>",
            "<:chip1:606205508600201226>",
            "<:champagne:606205300302938113>",
            "<:cash:606205470419451914>",
            "<:cards:606205447485259776>",
            "<:bar:606205272385388575>",
            "<:7_:606456701369188352>",
        ]

    async def cog_check(self, ctx):
        user = ctx.author.id

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT * FROM users WHERE(user_id = {user});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    return True
                else:
                    try:
                        await db.execute(
                            "INSERT INTO users(user_id, balance, exp, commands_run, job, tips) VALUES(?, 0, 0, 0, NULL, 1);",
                            [user]
                        )
                        await db.commit()
                        return True
                    except:
                        return False

    @commands.guild_only()
    @commands.command(aliases=["wallet", "money", "balance"])
    async def bal(self, ctx, member: discord.Member = None):
        """View your current coin balance"""
        if member is None:
            member_id = ctx.author.id
            member = ctx.author
        else:
            member_id = member.id
            member = member

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT balance FROM users WHERE(user_id = {member_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    emb = discord.Embed(
                        title=f"<:coin:599350279317487656> {member.display_name}'s balance:",
                        description=f"`{row[0]}` coins",
                        colour=0xFFD700,
                    )
                    await self.bot.send_embed(ctx, emb)
                    return

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(aliases=["bet"])
    async def gamble(self, ctx, amount: int = 0):
        """Gamble an amount of money for a small chance of winning more back"""
        cmd = self.bot.get_command("gamble")
        if amount <= 0:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You need to bet something.")
        else:
            amount_won = amount if random.randint(0, 100) < 22 else amount * -1
            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_bal = row[0]
                        if current_bal >= amount:
                            new_bal = current_bal + amount_won
                            await db.execute(
                                f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id}"
                            )
                            await db.commit()
                            if amount_won > 0:
                                await self.bot.send_embed(
                                    ctx,
                                    discord.Embed(
                                        title="<:coin:599350279317487656> Winner! <:coin:599350279317487656>",
                                        description=f"You won `{amount_won}` coins\n\nYou now have `{new_bal}` coins in your wallet",
                                        colour=0xFFD700,
                                    ).set_footer(text="Multiplier:  1x")
                                )
                                return
                            else:
                                await self.bot.send_embed(
                                    ctx,
                                    discord.Embed(
                                        title="<:coin:599350279317487656> Loser! <:coin:599350279317487656>",
                                        description=f"You lost `{abs(amount_won)}` coins\n\nYou now have `{new_bal}` coins in your wallet",
                                        colour=0xFF6961,
                                    )
                                )
                        else:
                            return await ctx.send(
                                "You do not have enough coins for this action."
                            )

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(aliases=["spin"])
    async def slots(self, ctx, amount: int = 0):
        """
        Bet an amount of money at the slot machine for a chance to win BIG
        There is no max bet amount
        """
        cmd = self.bot.get_command("slots")

        if amount <= 0:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You need to bet something.")
        slot_1 = random.randint(0, 9)
        slot_2 = random.randint(0, 9)
        slot_3 = random.randint(0, 9)

        if slot_1 == slot_2 and slot_1 == slot_3:
            amount_won = amount * 2
        elif slot_1 == slot_2 or slot_1 == slot_3 or slot_2 == slot_3:
            amount_won = amount
        else:
            amount_won = -amount

        if amount_won > 0:
            emb = discord.Embed(
                title="<:coin:599350279317487656> Winner! <:coin:599350279317487656>",
                description=f"You won `{amount_won}` coins from the slot machine",
                colour=0xFFD700,
            )
            emb.add_field(
                name="Outcome:",
                value=f"{self.slots_emojis[slot_1]}{self.slots_emojis[slot_2]}{self.slots_emojis[slot_3]}",
            )
        else:
            emb = discord.Embed(
                title="<:coin:599350279317487656> Loser! <:coin:599350279317487656>",
                description=f"You lost `{amount}` coins",
                colour=0xFF6961,
            )
            emb.add_field(
                name="Outcome:",
                value=f"{self.slots_emojis[slot_1]}{self.slots_emojis[slot_2]}{self.slots_emojis[slot_3]}",
            )

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    current_bal = row[0]
                    if current_bal < amount:
                        return await ctx.send(
                            "You do not have enough coins for this action."
                        )
                    else:
                        new_bal = current_bal + amount_won
                        await db.execute(
                            f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id}"
                        )
                        await db.commit()
                        await self.bot.send_embed(ctx, emb)
                        return

    @commands.guild_only()
    @commands.cooldown(1, 60, type=commands.BucketType.user)
    @commands.command()
    async def search(self, ctx):
        """Search the area for coins"""

        def weighted_number_generator():
            while True:
                num = random.random()
                weighted_num = round(num * num * 100)
                if weighted_num > 0:
                    return weighted_num

        money_found = weighted_number_generator()

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    current_bal = row[0]
                    new_bal = current_bal + money_found
                    await db.execute(
                        f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id};"
                    )
                    await db.commit()
                    msg = await ctx.send("Searching <a:loading:599345321084190760>")
                    await asyncio.sleep(2)
                    return await msg.edit(
                        content=f"You searched the area and found `{money_found}` coins which you added to your wallet."
                    )

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(aliases=["rich"])
    async def baltop(self, ctx):
        """View the top 5 richest users globally"""

        def render_username(
            user: typing.Union[discord.User, discord.Member], guild: discord.Guild
        ):
            """Properly handles username rendering for leaderboards"""
            if user in guild.members:
                member = guild.get_member(user.id)
                return member.display_name
            else:
                return str(user)

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                "SELECT user_id, balance FROM users ORDER BY balance DESC LIMIT 5"
            ) as cursor:
                rows = await cursor.fetchall()
                emb = discord.Embed(
                    title="<:coin:599350279317487656> Balance Leaderboard <:coin:599350279317487656>",
                    colour=0xFFD700,
                )
                for i in range(len(rows)):
                    user = self.bot.get_user(rows[i][0])
                    emb.add_field(
                        name=f"{i + 1}) {render_username(user, ctx.guild)}",
                        value=f"Balance: `{rows[i][1]}` coins",
                        inline=False,
                    )
                return await ctx.send(embed=emb)

    @commands.guild_only()
    @commands.cooldown(1, 20, type=commands.BucketType.user)
    @commands.command()
    async def trivia(self, ctx):
        """
        Get a random trivia question
        If you get it right you will get a small payout
        """
        colours = {"easy": 0x77DD77, "medium": 0xFFB347, "hard": 0xFF6961}
        value = {"easy": 3, "medium": 6, "hard": 9}

        trivia_url = "https://opentdb.com/api.php?amount=1&type=multiple"

        async with aiohttp.ClientSession() as session:
            resp = await session.get(trivia_url)
            data = await resp.json()

        question = data["results"][0]["question"]

        question = question.replace("&quot;", "")
        question = question.replace("&#039;", "'")
        question = question.replace("&rsquo;", "")

        category = data["results"][0]["category"]
        difficulty = data["results"][0]["difficulty"]
        correct_answer = data["results"][0]["correct_answer"]
        incorrect_answers = data["results"][0]["incorrect_answers"]
        incorrect_answers.append(correct_answer)
        all_answers = incorrect_answers

        choices = random.sample(all_answers, len(all_answers))
        prompt = "\n".join(
            f"**{i})** {choice}" for i, choice in enumerate(choices, start=1)
        )

        emb = discord.Embed(
            title=question,
            description=f"You have 12 seconds to answer\n\n{prompt}",
            colour=colours[difficulty],
        )
        emb.add_field(name="Difficulty", value=difficulty)
        emb.add_field(name="Value", value=value[difficulty])
        emb.add_field(name="Category", value=category)
        emb.set_author(
            icon_url=ctx.author.avatar_url,
            name=f"Trivia question for {ctx.author.name}",
        )
        emb.set_footer(text="You may answer with the number of the answer.")
        e = await ctx.send(embed=emb)

        try:
            msg = await self.bot.wait_for(
                "message",
                check=lambda m: m.author == ctx.author
                and m.channel == ctx.channel
                and m.content.isdigit(),
                timeout=12,
            )
        except asyncio.TimeoutError:
            emb.description = f"**Out of time.**\n\n{prompt}"
            return await e.edit(embed=emb)

        choice = None
        i = int(msg.content)

        if 0 < i <= len(choices):
            choice = choices[i - 1]

        if choice == correct_answer:
            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_bal = row[0]
                        new_bal = current_bal + value[difficulty]
                        await db.execute(
                            f"UPDATE users SET balance = {new_bal} WHERE user_id = {ctx.author.id};"
                        )
                        await db.commit()
                        return await ctx.send(
                            f"Correct! `{value[difficulty]}` coins have been added to your wallet"
                        )

        else:
            return await ctx.send(f"Wrong! The correct answer was `{correct_answer}`")

    @commands.guild_only()
    @commands.cooldown(1, 3600, type=commands.BucketType.user)
    @commands.command()
    async def steal(self, ctx, amount: int = 0, member: discord.Member = None):
        """
        Steal some coins from another user
        There is a chance that you'll be caught by the police and be fined
        This command has a cooldown of one hour
        Format: <p>steal <amount> @user
        """
        cmd = self.bot.get_command("steal")
        steal_success = True if random.randint(0, 100) < 30 else False

        if amount <= 0:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You need to specify an amount to steal.")
        elif member is None:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You need to specify someone to steal from.")
        elif member == ctx.author:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You cannot steal from yourself.")
        else:
            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        invoker_bal = row[0]
                        if invoker_bal < amount:
                            return await ctx.send(
                                "You cannot steal more coins than you have in your wallet."
                            )

                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {member.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is None:
                        return await ctx.send("Target does not have a wallet.")
                    else:
                        target_bal = row[0]
                        if target_bal < amount:
                            return await ctx.send(
                                "You cannot steal more coins than the target has in their wallet."
                            )
                        else:
                            if steal_success:
                                new_invoker_bal = invoker_bal + amount
                                new_target_bal = target_bal - amount
                                await db.execute(
                                    f"UPDATE users SET balance = {new_invoker_bal} WHERE user_id = {ctx.author.id};"
                                )
                                await db.execute(
                                    f"UPDATE users SET balance = {new_target_bal} WHERE user_id = {member.id};"
                                )
                                await db.commit()
                                await self.bot.send_embed(
                                    ctx,
                                    discord.Embed(
                                        title="<:coin:599350279317487656> Success!",
                                        description=f"You stole {amount} coins from {member.mention}, your new balance is {new_invoker_bal}",
                                        colour=0x77DD77,
                                    )
                                )
                                return
                            else:
                                new_invoker_bal = invoker_bal - amount
                                await db.execute(
                                    f"UPDATE users SET balance = {new_invoker_bal} WHERE user_id = {ctx.author.id};"
                                )
                                await db.commit()
                                await self.bot.send_embed(
                                    ctx,
                                    discord.Embed(
                                        title="<:coin:599350279317487656> Failure.",
                                        description=f"While trying to escape you were caught by the police.\nThey fined you {amount} coins leaving you with {new_invoker_bal} coins in your wallet.",
                                        colour=0xFF6961,
                                    )
                                )
                                return

    @commands.guild_only()
    @commands.command(aliases=["share"])
    async def give(self, ctx, amount: int = 0, member: discord.Member = None):
        if amount <= 0:
            return await ctx.send("You need to specify an amount to give.")
        elif member is None:
            return await ctx.send("You need to specify someone to give to.")
        elif member == ctx.author:
            return await ctx.send("You cannot give money to yourself.")
        else:
            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {ctx.author.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        invoker_bal = row[0]
                        if invoker_bal < amount:
                            return await ctx.send(
                                "You cannot give more coins than you have in your wallet."
                            )

                async with db.execute(
                    f"SELECT balance FROM users WHERE(user_id = {member.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is None:
                        return await ctx.send("Target does not have a wallet.")
                    else:
                        target_bal = row[0]
                        new_invoker_bal = invoker_bal - amount
                        new_target_bal = target_bal + amount
                        await db.execute(
                            f"UPDATE users SET balance = {new_invoker_bal} WHERE user_id = {ctx.author.id};"
                        )
                        await db.execute(
                            f"UPDATE users SET balance = {new_target_bal} WHERE user_id = {member.id};"
                        )
                        await db.commit()
                        await self.bot.send_embed(
                            ctx,
                            discord.Embed(
                                title="<:coin:599350279317487656> Success!",
                                description=f"You gave {amount} coins to {member.mention}, your new balance is {new_invoker_bal}",
                                colour=0x77DD77,
                            )
                        )
                        return


def setup(bot):
    bot.add_cog(Currency(bot))
