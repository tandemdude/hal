import discord
from discord.ext import commands
import datetime
import aiosqlite


class Announcements(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @commands.group(invoke_without_command=True)
    async def announcements(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```setchannel <channel> - sets the channel for announcements to be posted in\ndisable - disables announcements```"
        )

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @announcements.command()
    async def setchannel(self, ctx, channel: discord.TextChannel = None):
        """Set channel for announcements to be sent to"""
        if channel is None:
            return await ctx.send(
                "You need to specify a channel for announcements to appear in."
            )
        else:
            announcement_channel_id = channel.id

            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        await db.execute(
                            f"UPDATE guilds SET announcement_id = {announcement_channel_id} WHERE guild_id = {ctx.guild.id}"
                        )
                        await db.commit()
                    else:
                        await ctx.send(
                            "Please wait, creating a record for you in the database."
                        )
                        await db.execute(
                            f"INSERT INTO guilds(guild_id, welcome_id, role_id, logging_id, suggestion_id, announcement_id) VALUES({ctx.guild.id}, NULL, NULL, NULL, NULL, {announcement_channel_id});"
                        )
                        await db.commit()
            return await ctx.send("Announcement channel registered.")

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @announcements.command()
    async def disable(self, ctx):
        """Disable announcements in the guild"""
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send(
                        "No record found. Contact my creator if you believe this to be incorrect."
                    )
                else:
                    await ctx.send("Disabling suggestions...")
                    await db.execute(
                        f"UPDATE guilds SET announcement_id = NULL WHERE guild_id = {ctx.guild.id}"
                    )
                    await db.commit()
                    return await ctx.send("Announcements disabled successfully.")

    @commands.guild_only()
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    @commands.bot_has_permissions(mention_everyone=True)
    @commands.has_permissions(mention_everyone=True)
    @commands.command()
    async def announce(self, ctx, announcement=None):
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT announcement_id FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row[0] is not None:
                    chan_id = row[0]
                    channel = ctx.guild.get_channel(chan_id)
                else:
                    return await ctx.send(
                        "This server does not have announcements enabled."
                    )

        if announcement is None:
            return await ctx.send("You need to include something to announce.")
        elif len(announcement) <= 2048:
            emb = discord.Embed(
                title=f"Announcement from {ctx.author.name}:",
                description=announcement,
                colour=0xEC6761,
            )
            msg = await channel.send(content="@everyone", embed=emb)
            return await ctx.send(
                "<:tickYes:315009125694177281> Announcement sent successfully."
            )
        else:
            return await ctx.send(
                "<:tickNo:315009174163685377> Your announcement is too long! It must be less than 2048 characters."
            )


def setup(bot):
    bot.add_cog(Announcements(bot))
