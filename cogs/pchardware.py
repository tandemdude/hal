import discord
from discord.ext import commands
import libneko
import aiosqlite


def create_ranked_benchmark_embed(
    type, part_num, brand, model, rank, bench, samples, url
):
    e = discord.Embed(
        title=f"{type} Rankings",
        description=f"Rank: **{rank}**\nBrand: {brand}\nModel: [{model}]({url})\nPart Number: `{part_num}`",
        colour=0xEC6761,
    )
    e.add_field(name="Benchmark Score:", value=f"{bench}", inline=True)
    e.add_field(name="Test Reliability:", value=f"{samples} tests")
    return e


def create_benchmark_embed(type, part_num, brand, model, rank, bench, samples, url):
    e = discord.Embed(
        title=f"Part: {type}",
        description=f"Rank: **{rank}**\nBrand: {brand}\nModel: [{model}]({url})\nPart Number: `{part_num}`",
        colour=0xEC6761,
    )
    e.add_field(name="Benchmark Score:", value=f"{bench}", inline=True)
    e.add_field(name="Test Reliability:", value=f"{samples} tests")
    return e


class Benchmarks(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db = "hardware_data.db"

    @commands.guild_only()
    @commands.group(invoke_without_command=True)
    async def hwsearch(self, ctx):
        await ctx.send(
            f"Available Categories to Search:\n```\ntype\npartnum\nbrand\nmodel\nrank```\nRun `{ctx.prefix}help hwsearch <category>` to view general information and help about searching within that category.\nAll data taken from `https://www.userbenchmark.com/`\n__Use your discretion when evaluating this information, I am not responsible for any inaccuracies.__"
        )

    @commands.guild_only()
    @hwsearch.command(name="type")
    async def _type(self, ctx, query_param: str = None, limit: int = 5):
        """
        Search the benchmark database for a specific type of hardware.
        Options available to query:
        CPU|GPU|SSD|HDD|USB|RAM

        Example usage:
        `<p>hwsearch type CPU`
        This would return the stats for the top 10 benchmarked CPUs

        The amount of results returned can be changed with the limit param,
        if unspecified, the limit will default to 5, max limit is 15.
        """
        possible_types = ["CPU", "GPU", "SSD", "HDD", "USB", "RAM"]

        if limit < 1 or limit > 15:
            return await ctx.send("Invalid limit specified.")
        if query_param is None:
            return await ctx.send("No hardware type specified.")
        elif query_param.upper() not in possible_types:
            return await ctx.send("Invalid hardware type specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute(
                "SELECT * FROM hardware WHERE type = ? ORDER BY rank ASC LIMIT ?",
                [query_param.upper(), limit],
            ) as cursor:
                rows = await cursor.fetchall()

                output_embs = []
                for row in rows:
                    output_embs.append(create_ranked_benchmark_embed(*row))

        nav = libneko.pag.navigator.EmbedNavigator(ctx, output_embs)
        nav.start()

    @commands.guild_only()
    @hwsearch.command(aliases=["pnum", "part_num", "pn"])
    async def partnum(self, ctx, *, part_num: str = None):
        """
        Search the benchmark database for a specific part number or segment of a part number.
        Command will return the first match it could find in the database.

        Example usage:
        `<p>hwsearch partnum BX80684I99900KF`
        This would return the stats for an Intel i9-9900KF
        """
        if part_num is None:
            return await ctx.send("No part number specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute(
                "SELECT * FROM hardware WHERE part_num LIKE ?", [f"%{part_num}%"]
            ) as cursor:
                row = await cursor.fetchone()

                return await ctx.send(embed=create_benchmark_embed(*row))

    @commands.guild_only()
    @hwsearch.command()
    async def brand(self, ctx, brand: str = None, hwtype: str = None):
        """
        Search the benchmark database for a specific brand of hardware.
        Optionally search for a specific type of hardware from that brand.

        Options available for type:
        CPU|GPU|SSD|HDD|USB|RAM

        Example usage:
        `<p>hwsearch brand AMD`
        This would return the first 5 products found which are made by AMD
        """
        possible_types = ["CPU", "GPU", "SSD", "HDD", "USB", "RAM"]

        if brand is None:
            return await ctx.send("No brand specified.")
        if hwtype is not None and hwtype.upper() not in possible_types:
            return await ctx.send("Invalid hardware type specified.")

        async with aiosqlite.connect(self.db) as db:
            if hwtype is None:
                async with db.execute(
                    "SELECT * FROM hardware WHERE brand LIKE ? LIMIT 5", [f"%{brand}%"]
                ) as cursor:
                    rows = await cursor.fetchall()

            else:
                async with db.execute(
                    "SELECT * FROM hardware WHERE type = ? AND brand LIKE ? LIMIT 5",
                    [hwtype.upper(), f"%{brand}%"],
                ) as cursor:
                    rows = await cursor.fetchall()

            if rows is not None:
                output_embs = []
                for row in rows:
                    output_embs.append(create_benchmark_embed(*row))
            else:
                return await ctx.send("No results were found.")

        nav = libneko.pag.navigator.EmbedNavigator(ctx, output_embs)
        nav.start()

    @commands.guild_only()
    @hwsearch.command()
    async def model(self, ctx, *, model: str = None):
        """
        Search the benchmark database for a specific model of hardware.
        You may provide an exact model or part of a model and the query will return the first match found.

        Example usage:
        `<p>hwsearch model Ryzen 7 3700X`
        This would return the data for the Ryzen 7 3700X
        """
        if model is None:
            return await ctx.send("No model specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute(
                "SELECT * FROM hardware WHERE model LIKE ?", [f"%{model}%"]
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send("No results were found.")
                else:
                    return await ctx.send(embed=create_benchmark_embed(*row))

    @commands.guild_only()
    @hwsearch.command()
    async def rank(self, ctx, rank: int = None, hwtype: str = None):
        """
        Search the benchmark database for the hardware at a specific rank for a specific type.
        Both rank and type must be provided.

        Options available for type:
        CPU|GPU|SSD|HDD|USB|RAM

        Example usage:
        <p>hwsearch rank 1 RAM
        This would return the data for the RAM holding the number 1 rank
        """
        possible_types = ["CPU", "GPU", "SSD", "HDD", "USB", "RAM"]

        if rank is None or rank < 0:
            return await ctx.send("Invalid rank specified.")
        if hwtype.upper() not in possible_types:
            return await ctx.send("Invalid hardware type specified.")

        async with aiosqlite.connect(self.db) as db:
            async with db.execute(
                "SELECT * FROM hardware WHERE rank = ? AND type = ?", [rank, hwtype]
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send("No results were found.")
                else:
                    return await ctx.send(embed=create_benchmark_embed(*row))


def setup(bot):
    bot.add_cog(Benchmarks(bot))
