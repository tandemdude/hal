# Bot owner commands extension made by github u/tandemdude
# https://github.com/tandemdude
from discord.ext import commands
import discord
import libneko
import os
import aiosqlite
import re


class Owner(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner()
    async def kill(self, ctx):
        """Disconnects the bot from discord and stop the script"""
        await ctx.send("Bot logging out.")
        await self.bot.logout()

    @commands.command()
    @commands.is_owner()
    async def temp(self, ctx):
        """Gets the current CPU temp of the raspberry pi"""
        temp = os.popen("vcgencmd measure_temp").read()
        temp_stripped = re.findall(r"(\d+(?:\.\d+)?)", temp)
        current_temp = temp_stripped[0]
        await ctx.send(
            embed=discord.Embed(
                title=":thermometer: Current operating temperature:",
                description=f"{current_temp}°C",
                colour=0xFF0000,
            )
        )

    @commands.command(aliases=["lg", "guilds"])
    @commands.is_owner()
    async def listguilds(self, ctx):
        await ctx.trigger_typing()
        nav = libneko.pag.StringNavigatorFactory(
            max_lines=15, prefix="```", suffix="```"
        )
        for i, guild in enumerate(self.bot.guilds, 1):
            nav.add_line(f"{i}) {guild.name} - {len(guild.members)}")
        nav.start(ctx)

    @commands.group(invoke_without_command=True)
    @commands.is_owner()
    async def ownerprune(self, ctx):
        return await ctx.send("Available subcommands:\n```\ncoins\nxp```")

    @ownerprune.command()
    @commands.is_owner()
    async def coins(self, ctx, user: discord.User = None, amount_to_remove: int = 0):
        if user is None:
            return await ctx.send("No user specified.")
        else:
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    "SELECT balance FROM users WHERE user_id = ?;", [user.id]
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_bal = row[0]
                        new_bal = (
                            0
                            if amount_to_remove == 0
                            else max(0, current_bal - amount_to_remove)
                        )
                        await db.execute(
                            "UPDATE users SET balance = ? WHERE user_id = ?",
                            [new_bal, user.id],
                        )
                        await db.commit()
                        return await ctx.send("User's balance updated successfully.")
                    else:
                        return await ctx.send("User's record not found.")

    @ownerprune.command()
    @commands.is_owner()
    async def xp(self, ctx, user: discord.User = None, amount_to_remove: int = 0):
        if user is None:
            return await ctx.send("No user specified.")
        else:
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    "SELECT exp FROM users WHERE user_id = ?;", [user.id]
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_xp = row[0]
                        new_xp = (
                            0
                            if amount_to_remove == 0
                            else max(0, current_xp - amount_to_remove)
                        )
                        await db.execute(
                            "UPDATE users SET exp = ? WHERE user_id = ?",
                            [new_xp, user.id],
                        )
                        await db.commit()
                        return await ctx.send("User's xp updated successfully.")
                    else:
                        return await ctx.send("User's record not found.")

    @commands.group(invoke_without_command=True)
    @commands.is_owner()
    async def ownergive(self, ctx):
        return await ctx.send("Available subcommands:\n```\ncoins\nxp```")

    @ownergive.command()
    @commands.is_owner()
    async def coins(self, ctx, user: discord.User = None, amount_to_give: int = 0):
        if user is None:
            return await ctx.send("No user specified.")
        if amount_to_give < 1:
            return await ctx.send("No amount specified.")
        else:
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    "SELECT balance FROM users WHERE user_id = ?;", [user.id]
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_bal = row[0]
                        new_bal = current_bal + amount_to_give
                        await db.execute(
                            "UPDATE users SET balance = ? WHERE user_id = ?",
                            [new_bal, user.id],
                        )
                        await db.commit()
                        return await ctx.send("User's balance updated successfully.")
                    else:
                        return await ctx.send("User's record not found.")

    @ownergive.command()
    @commands.is_owner()
    async def xp(self, ctx, user: discord.User = None, amount_to_give: int = 0):
        if user is None:
            return await ctx.send("No user specified.")
        if amount_to_give < 1:
            return await ctx.send("No amount specified")
        else:
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    "SELECT exp FROM users WHERE user_id = ?;", [user.id]
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        current_xp = row[0]
                        new_xp = current_xp + amount_to_give
                        await db.execute(
                            "UPDATE users SET exp = ? WHERE user_id = ?",
                            [new_xp, user.id],
                        )
                        await db.commit()
                        return await ctx.send("User's xp updated successfully.")
                    else:
                        return await ctx.send("User's record not found.")

    @commands.guild_only()
    @commands.is_owner()
    @commands.command()
    async def cmdstats(self, ctx):
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                "SELECT name, uses FROM cmdstats ORDER BY uses DESC LIMIT 10"
            ) as cursor:
                rows = await cursor.fetchall()

                text = ""
                for row in rows:
                    text += f"**{row[0]}** - `{row[1]}` uses\n"
                embed = discord.Embed(
                    title="Most used commands:", description=text, colour=0xEC6761
                )
                await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Owner(bot))
