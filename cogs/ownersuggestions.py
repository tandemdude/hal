import discord
from discord.ext import commands
import aiohttp


class Suggestions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.suggestion_channel_id = 606285474948055041
        self.bug_channel_id = 606285493507981323
        self.guild_id = 606204996857495562

    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 30, type=commands.BucketType.user)
    async def suggestfeature(self, ctx, *, suggestion=None):
        """Suggest a feature to my creator, cooldown of 30 seconds to prevent spamming."""
        g = self.bot.get_guild(self.guild_id)
        c = self.bot.get_channel(self.suggestion_channel_id)
        whs = await c.webhooks()
        if suggestion is None:
            await ctx.send("You need to include something to suggest.")
        elif len(suggestion) <= 2048:
            emb = discord.Embed(
                title="New Suggestion:", description=suggestion, colour=0x77DD77
            )
            emb.set_footer(
                icon_url=ctx.author.avatar_url, text=f"Suggested by {str(ctx.author)}"
            )
            msg = await whs[0].send(
                embed=emb,
                username="HAL Suggestions",
                avatar_url=self.bot.user.avatar_url,
            )
            await msg.add_reaction("⬆")
            await msg.add_reaction("⬇")
            await ctx.send(
                "<:tickYes:315009125694177281> Suggestion sent successfully."
            )
        else:
            await ctx.send(
                "<:tickNo:315009174163685377> Your suggestion is too long! It must be less than 2048 characters."
            )

    @commands.guild_only()
    @commands.command()
    @commands.cooldown(1, 30, type=commands.BucketType.user)
    async def reportbug(self, ctx, *, bug=None):
        """Report a bug to my creator, cooldown of 30 seconds to prevent spamming."""
        g = self.bot.get_guild(self.guild_id)
        c = self.bot.get_channel(self.bug_channel_id)
        whs = await c.webhooks()
        if bug is None:
            await ctx.send("You need to include a bug to report.")
        elif len(bug) <= 2048:
            emb = discord.Embed(
                title="New Bug Report:", description=bug, colour=0xEC6761
            )
            emb.set_footer(
                icon_url=ctx.author.avatar_url, text=f"Reported by {str(ctx.author)}"
            )
            await whs[0].send(
                embed=emb, username="HAL Bugs", avatar_url=self.bot.user.avatar_url
            )
            await ctx.send("<:tickYes:315009125694177281> Report sent successfully.")
        else:
            await ctx.send(
                "<:tickNo:315009174163685377> Your report is too long! It must be less than 2048 characters."
            )


def setup(bot):
    bot.add_cog(Suggestions(bot))
