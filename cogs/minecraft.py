import discord
import aiohttp, asyncio
from discord.ext import commands


class MinecraftUtils(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.group(invoke_without_command=True, aliases=["minecraft"])
    async def mc(self, ctx):
        """Show available subcommands"""
        await ctx.send(
            "Available Subcommands:\n```skin <name> - Get the skin for a specific username\nuuid <name> - Get the UUID for a specific username```"
        )

    @commands.guild_only()
    @mc.command()
    async def skin(self, ctx, username=None):
        """Get the skin of a username or UUID"""
        if username == None:
            await ctx.send("You need to specify a username!")
        else:
            url = f"https://minotar.net/armor/body/{username}.png"
            embed = discord.Embed(
                title=f"{username}'s Minecraft Skin:", colour=0x00FF00
            )
            embed.set_image(url=url)
            embed.set_footer(text=f"Requested by {ctx.author}")
            await self.bot.send_embed(ctx, embed)

    @commands.guild_only()
    @mc.command()
    async def uuid(self, ctx, username=None):
        """Get the UUID of a specified username"""
        if username == None:
            await ctx.send("You need to specify a username!")
        else:
            url = f"https://api.minetools.eu/uuid/{username}"
            async with aiohttp.ClientSession() as session:
                resp = await session.get(url)
                data = await resp.json()
            embed = discord.Embed(
                title=f"{username}'s UUID:", description=data["id"], colour=0x00FF00
            )
            embed.set_author(
                name=username, icon_url=f"https://minotar.net/helm/{username}.png"
            )
            embed.set_footer(text=f"Requested by {ctx.author}")
            await self.bot.send_embed(ctx, embed)


def setup(bot):
    bot.add_cog(MinecraftUtils(bot))
