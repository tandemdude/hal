import discord
from discord.ext import commands
import aiosqlite
import math
import typing


def calc_level_from_xp(xp: int):
    return int(math.floor(0.2 * math.sqrt(xp)))


def calc_xp_for_level(level: int):
    return int((level / 0.2) ** 2)


class Levelling(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def cog_check(self, ctx):
        user = ctx.author.id

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT * FROM users WHERE(user_id = {user});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    return True
                else:
                    try:
                        await db.execute(
                            "INSERT INTO users(user_id, balance, exp, commands_run, job, tips) VALUES(?, 0, 0, 0, NULL, 1);",
                            [user]
                        )
                        await db.commit()
                        return True
                    except:
                        return False

    @commands.guild_only()
    @commands.command(aliases=["level"])
    async def rank(self, ctx, member: discord.Member = None):
        member = ctx.author if member is None else member

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT exp FROM users WHERE(user_id = {member.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    current_xp = row[0]
                    current_level = calc_level_from_xp(current_xp)
                    xp_for_next_level = calc_xp_for_level(current_level + 1)
                    xp_to_next_level = xp_for_next_level - current_xp
                    await self.bot.send_embed(
                        ctx,
                        discord.Embed(
                            title=f"{member.display_name}'s Stats",
                            description=f"Level {current_level}\n{current_xp}/{xp_for_next_level} xp\n{xp_to_next_level} xp to the next level",
                            colour=0xEC6761,
                        ).set_thumbnail(url=member.avatar_url)
                    )
                    return

    @commands.guild_only()
    @commands.command(aliases=["leveltop", "xptop", "exptop"])
    async def levels(self, ctx):
        """View the top 5 highest level users globally"""

        def render_username(
            user: typing.Union[discord.User, discord.Member], guild: discord.Guild
        ):
            """Properly handles username rendering for leaderboards"""
            if user in guild.members:
                member = guild.get_member(user.id)
                return member.display_name
            else:
                return str(user)

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                "SELECT user_id, exp FROM users ORDER BY exp DESC LIMIT 5"
            ) as cursor:
                rows = await cursor.fetchall()
                emb = discord.Embed(title="Level Leaderboard", colour=0xEC6761)
                emb.set_thumbnail(url="https://i.imgur.com/Tdd4fTK.png")

                for i in range(len(rows)):
                    user = self.bot.get_user(rows[i][0])
                    emb.add_field(
                        name=f"{i + 1}) {render_username(user, ctx.guild)}",
                        value=f"Level: **{calc_level_from_xp(rows[i][1])}**, with **{rows[i][1]}** xp",
                        inline=False,
                    )
                return await ctx.send(embed=emb)


def setup(bot):
    bot.add_cog(Levelling(bot))
