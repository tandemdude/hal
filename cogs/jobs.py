import discord
from discord.ext import commands
import aiosqlite

JOBS = ["binman", "janitor", "chef", "streamer", "boxer", "astronaut"]
JOB_REQUIREMENTS = {
    "binman": 0,
    "janitor": 100,
    "chef": 250,
    "streamer": 500,
    "boxer": 1000,
    "astronaut": 5000,
}
JOB_VALUES = {
    "binman": 50,
    "janitor": 100,
    "chef": 150,
    "streamer": 250,
    "boxer": 350,
    "astronaut": 450,
}


class Jobs(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.jobs = [
            ("binman", 0, 50),
            ("janitor", 100, 100),
            ("chef", 250, 150),
            ("streamer", 500, 250),
            ("boxer", 1000, 350),
            ("astronaut", 5000, 450),
        ]
        self.available = "<:tickYes:315009125694177281>"
        self.unavailable = "<:tickNo:315009174163685377>"

    @commands.guild_only()
    @commands.cooldown(1, 3600, type=commands.BucketType.user)
    @commands.command()
    async def work(self, ctx):
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT job, balance FROM users WHERE(user_id = {ctx.author.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    job = JOBS[row[0]]
                    wage = JOB_VALUES[job]
                    new_money_amount = wage + row[1]

                    await db.execute(
                        f"UPDATE users SET balance = {new_money_amount} WHERE user_id = {ctx.author.id}"
                    )
                    await db.commit()

                    return await ctx.send(
                        f"Well done {ctx.author.display_name}, you did your job well. You've been paid `{wage}` coins."
                    )
                else:
                    return await ctx.send(
                        "How are you gonna work while unemployed?\nTip: get a job first, you can see available jobs with `{ctx.prefix}jobs`."
                    )

    @commands.guild_only()
    @commands.command()
    async def jobs(self, ctx):
        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT commands_run FROM users WHERE(user_id = {ctx.author.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    total_commands_run = row[0]
                else:
                    total_commands_run = 1

                e = discord.Embed(
                    title="Available Jobs:",
                    description=f"Run `{ctx.prefix}pickjob <jobname>` to get hired for a job\n**Key:**\n{self.available}: available, {self.unavailable}: unavailable",
                    colour=0xEC6761,
                )

                for job, reqd_cmds, value in self.jobs:
                    if total_commands_run >= reqd_cmds:
                        emoji = self.available
                    else:
                        emoji = self.unavailable

                    e.add_field(
                        name=f"{emoji} {job.title()}", value=f"Wage: `{value}` coins"
                    )

                await self.bot.send_embed(ctx, e)

    @commands.guild_only()
    @commands.cooldown(1, 86400, type=commands.BucketType.user)
    @commands.command()
    async def pickjob(self, ctx, job: str = None):
        cmd = self.bot.get_command("pickjob")
        if job is None:
            cmd.reset_cooldown(ctx)
            return await ctx.send("You need to specify a job type.")
        elif job.casefold() not in JOBS:
            cmd.reset_cooldown(ctx)
            return await ctx.send("That job type does not exist.")
        else:
            jobnum = JOBS.index(job.lower())
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute(
                    f"SELECT commands_run, job FROM users WHERE(user_id = {ctx.author.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row[1] == JOBS.index(job.lower()):
                        cmd.reset_cooldown(ctx)
                        return await ctx.send("You are already working at this job.")

                    if row is not None:
                        if row[0] >= JOB_REQUIREMENTS[job.lower()]:
                            await db.execute(
                                f"UPDATE users SET job = {jobnum} WHERE user_id = {ctx.author.id}"
                            )
                            await db.commit()
                            await self.bot.send_embed(
                                ctx,
                                discord.Embed(
                                    title="Hired!",
                                    description=f"You have been hired for the job: `{job.title()}`\nYou can work once an hour using `{ctx.prefix}work`",
                                    colour=0x77DD77,
                                )
                            )
                            return
                        else:
                            cmd.reset_cooldown(ctx)
                            return await ctx.send(
                                "You do not meet the requirements for this job yet. Use the bot more to unlock it."
                            )
                    else:
                        await db.execute(
                            "INSERT INTO users(user_id, balance, exp, commands_run, job, tips) VALUES(?, 0, 0, 0, NULL, 1);",
                            [ctx.author.id]
                        )
                        await db.commit()
                        cmd.reset_cooldown(ctx)
                        return await ctx.send(
                            "You do not meet the requirements for this job yet. Use the bot more to unlock it."
                        )


def setup(bot):
    bot.add_cog(Jobs(bot))
