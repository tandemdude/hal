import discord
from discord.ext import commands
import datetime
import aiosqlite


class Welcome(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db_file = "botdata.db"

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @commands.group(invoke_without_command=True)
    async def welcome(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```setchannel <channel> - sets the channel for join/leave messages to be posted in\ndisable - disables join/leave messages```"
        )

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @welcome.command()
    async def setchannel(self, ctx, channel: discord.TextChannel = None):
        """Set channel for welcome messages to be sent to"""
        if channel is None:
            return await ctx.send(
                "You need to specify a channel for welcome messages to appear in."
            )
        else:
            welcome_channel_id = channel.id

            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        await db.execute(
                            f"UPDATE guilds SET welcome_id = {welcome_channel_id} WHERE guild_id = {ctx.guild.id}"
                        )
                        await db.commit()
                    else:
                        await ctx.send(
                            "Please wait, creating a record for you in the database."
                        )
                        await db.execute(
                            f"INSERT INTO guilds(guild_id, welcome_id, role_id, logging_id, suggestion_id, announcement_id) VALUES({ctx.guild.id}, {welcome_channel_id}, NULL, NULL, NULL, NULL);"
                        )
                        await db.commit()
            return await ctx.send("Welcome channel registered.")

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @welcome.command()
    async def disable(self, ctx):
        """Disable welcome messages in the guild"""
        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send(
                        "No record found. Contact my creator if you believe this to be incorrect."
                    )
                else:
                    await ctx.send("Disabling welcome messages...")
                    await db.execute(
                        f"UPDATE guilds SET welcome_id = NULL WHERE guild_id = {ctx.guild.id}"
                    )
                    await db.commit()
                    return await ctx.send("Welcome messages disabled successfully.")

    @commands.Cog.listener()
    async def on_member_join(self, member):
        guild_id = member.guild.id

        embed = discord.Embed(
            title=f"Welcome to {member.guild.name} :wave:",
            colour=0xFF6961,
            timestamp=datetime.datetime.utcnow(),
        )
        embed.set_author(name=str(member), icon_url=member.avatar_url)

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT welcome_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    welcome_channel_id = row[0]
                    try:
                        welcome_channel = member.guild.get_channel(welcome_channel_id)
                        await welcome_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        guild_id = member.guild.id

        embed = discord.Embed(
            title=f"{str(member)} has left us :cry:",
            colour=0xFF6961,
            timestamp=datetime.datetime.utcnow(),
        )
        embed.set_author(name=str(member), icon_url=member.avatar_url)

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT welcome_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    welcome_channel_id = row[0]
                    try:
                        welcome_channel = member.guild.get_channel(welcome_channel_id)
                        await welcome_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass


def setup(bot):
    bot.add_cog(Welcome(bot))
