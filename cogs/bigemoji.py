import discord
from discord.ext import commands
import aiohttp
import io
from libneko import pag


class Emojis(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command()
    async def bigemoji(self, ctx, *, emoji: discord.Emoji):
        emoji_url = str(emoji.url)
        async with aiohttp.ClientSession() as session:
            async with session.get(emoji_url) as resp:
                resp.raise_for_status()
                data = await resp.read()

        data = io.BytesIO(data)
        data.seek(0)

        await ctx.send(emoji.name, file=discord.File(data, f"{emoji.name}.{emoji.url}"))

    @commands.guild_only()
    @commands.command(aliases=["emojilib"], hidden=True)
    async def emojilibrary(self, ctx):
        nav = pag.EmbedNavigator_factory(max_lines=8)
        for emoji in bot.emojis:
            nav.add_line(str(emoji))
        nav.start(ctx)


def setup(bot):
    bot.add_cog(Emojis(bot))
