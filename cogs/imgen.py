import discord
from discord.ext import commands
import configparser
import io
import mimetypes
import aiohttp

parser = configparser.ConfigParser()
parser.read("TOKENS.INI")
TOKEN = parser["DEFAULT"]["dankmemer"]


class Imgen(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.auth_header = {"Authorization": f"{TOKEN}"}

    @commands.guild_only()
    @commands.command()
    async def airpods(self, ctx, member: discord.Member = None):
        link = "https://dankmemer.services/api/airpods"
        avatar_url = member.avatar_url if member is not None else ctx.author.avatar_url
        async with aiohttp.ClientSession() as session:
            async with session.post(
                link, headers=self.auth_header, json={"avatars": [str(avatar_url)]}
            ) as resp:
                await resp.raise_for_status()
                print("response below")
                print(await resp.read())
                image = io.BytesIO(await resp.read())
                image.seek(0)
                file_name = "image.png"
                await ctx.send(file=discord.File(image, file_name))


def setup(bot):
    bot.add_cog(Imgen(bot))
