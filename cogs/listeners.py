from discord.ext import commands
import discord
import random
import asyncio


class Listeners(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.guild_id = 606204996857495562
        self.vote_channel_id = 608054039938334730

    @commands.Cog.listener()
    async def on_message(self, message):
        if (
            self.bot.user in message.mentions
            and not message.author.bot
            and ("prefix" in message.content or "help" in message.content or self.bot.user.mention == message.content)
        ):
            await message.channel.send(
                "Beep Boop! My command prefix is `h.`, you can view available commands with `h.help`."
            )
        if (
            not message.author.bot
            and not message.content.startswith("h.")
            and message.guild.id != 264445053596991498
        ):
            if random.randint(0, 10) < 3:
                xp_gained = random.randint(1, 10)
                await self.bot.give_xp(amount=xp_gained, id=message.author.id)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        error = error.__cause__ or error
        if isinstance(error, commands.errors.CommandNotFound):
            if ctx.invoked_with == "chat":
                await ctx.send(
                    "That command is currently unloaded. Please wait while the extension is initialised."
                )
                self.bot.load_extension("cogs.ChatAI")
                await asyncio.sleep(1)
                await ctx.send("Extension loaded successfully.")
            elif ctx.author.id == self.bot.owner_id:
                await ctx.message.add_reaction("❓")
        elif isinstance(error, commands.errors.MissingPermissions):
            await ctx.send(
                embed=discord.Embed(
                    title="Missing Permissions.",
                    description=f'You don\'t have the right permissions for that command.\n**Permission(s) missing:**\n{", ".join(error.missing_perms)}',
                    colour=0xEC6761,
                )
            )
        elif isinstance(error, commands.errors.BotMissingPermissions):
            await ctx.send(
                embed=discord.Embed(
                    title="Missing Permissions.",
                    description=f'I don\'t have the correct permissions to carry out that action.\n**Permission(s) missing:**\n{", ".join(error.missing_perms)}',
                    colour=0xEC6761,
                )
            )
        elif isinstance(error, commands.errors.CommandOnCooldown):
            await ctx.send(
                embed=discord.Embed(
                    title="Slow it down bud.",
                    description=f"That command is on cooldown.\nYou must wait `{round(error.retry_after)}` seconds before using it again.",
                    colour=0xEC6761,
                )
            )
        elif isinstance(error, commands.errors.BadArgument):
            await ctx.send(
                embed=discord.Embed(
                    title="Bad Argument.",
                    description="Looks like you gave me a bad argument. If you think this error is a mistake contact my creator.",
                    colour=0xEC6761,
                )
            )
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.send(
                embed=discord.Embed(
                    title="Missing Argument.",
                    description=f"Looks like you didn't give me some of the arguments\n**Argument(s) missing:**\n{error.param.name}",
                    colour=0xEC6761,
                )
            )
        else:
            raise error

    @commands.Cog.listener()
    async def on_command(self, ctx):
        user_id = ctx.author.id
        await self.bot.increment_commands_run(user_id)
        if user_id != 215061635574792192:
            await self.bot.update_commands_stats(ctx)


def setup(bot):
    bot.add_cog(Listeners(bot))
