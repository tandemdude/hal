# On ready listener extension coded by github u/tandemdude
# https://github.com/tandemdude
import discord
import json
import asyncio
from discord.ext import commands


class OnReady(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.prefix = "t!"

    @commands.Cog.listener()
    async def on_ready(self):
        print("The bot is ready!")
        while True:
            await self.bot.change_presence(
                activity=discord.Activity(
                    type=discord.ActivityType.watching,
                    name=f"over {len([*self.bot.get_all_members()])} users",
                )
            )
            await asyncio.sleep(60)


def setup(bot):
    bot.add_cog(OnReady(bot))
