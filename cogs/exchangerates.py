import discord
from discord.ext import commands
import aiohttp
import datetime
from prettytable import PrettyTable

ABBREVIATIONS = [
    "CAD",
    "HKD",
    "ISK",
    "PHP",
    "DKK",
    "HUF",
    "CZK",
    "AUD",
    "RON",
    "SEK",
    "IDR",
    "INR",
    "BRL",
    "RUB",
    "HRK",
    "JPY",
    "THB",
    "CHF",
    "SGD",
    "PLN",
    "BGN",
    "TRY",
    "CNY",
    "NOK",
    "NZD",
    "ZAR",
    "USD",
    "MXN",
    "ILS",
    "GBP",
    "KRW",
    "MYR",
    "EUR",
]

CURRENCIES = [
    "Canadian Dollar",
    "Hong Kong Dollar",
    "Icelandic Krona",
    "Filipino Peso",
    "Danish Krone",
    "Hungarian Forint",
    "Czech Koruna",
    "Australian Dollar",
    "Romanian New Lei",
    "Swedish Krona",
    "Indonesian Rupiah",
    "Indian Rupee",
    "Brazillian Real",
    "Russian Rouble",
    "Croatian Kuna",
    "Japanese Yen",
    "Thai Baht",
    "Swiss Franc",
    "Singaporean Dollar",
    "Polish Zloty",
    "Bulgarian Lev",
    "Turkish New Lira",
    "Chinese Yuan",
    "Norwegian Kroner",
    "New Zealand Dollar",
    "South African Rand",
    "United States Dollar",
    "Mexican Peso",
    "Israeli New Shekel",
    "Great British Pound",
    "South Korean Won",
    "Malaysian Ringgit",
    "Euro",
]


class ExchangeRates(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.base_url = "https://api.exchangeratesapi.io/"

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command()
    async def rates(self, ctx, base: str = "USD"):
        """
        Retrieve a list of all current exchange rates from a base currency
        Base currency defaults to USD (united states dollar)
        Options for base currency:
            - CAD, HKD, ISK, PHP, DKK, HUF
            - CZK, AUD, RON, SEK, IDR, INR
            - BRL, RUB, HRK, JPY, THB, CHF
            - SGD, PLN, BGN, TRY, CNY, NOK
            - NZD, ZAR, USD, MXN, ILS, GBP
            - KRW, MYR, EUR
        """
        if base.upper() not in ABBREVIATIONS:
            return await ctx.send(
                f"The base you provided was invalid, to view valid currencies you can use `{ctx.prefix}currencies`"
            )

        td = datetime.timedelta(days=2)
        yesterday = datetime.datetime.utcnow() - td
        yesterday_formatted = yesterday.strftime("%Y-%m-%d")
        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{self.base_url}latest?base={base.upper()}"
            ) as resp:
                resp.raise_for_status()
                current_data = await resp.json()
            async with session.get(
                f"{self.base_url}{yesterday_formatted}?base={base.upper()}"
            ) as resp:
                resp.raise_for_status()
                yesterday_data = await resp.json()

        table = PrettyTable(["Currency", "Rate", "Trend"])
        for rate in current_data["rates"]:
            val = current_data["rates"][rate]
            rounded = round(val, 3)
            if current_data["rates"][rate] > yesterday_data["rates"][rate]:
                table.add_row([rate, rounded, "⬆"])
            elif current_data["rates"][rate] == yesterday_data["rates"][rate]:
                table.add_row([rate, rounded, "="])
            elif current_data["rates"][rate] < yesterday_data["rates"][rate]:
                table.add_row([rate, rounded, "⬇"])
        await ctx.send(
            embed=discord.Embed(
                title=f"Today's exchange rates based off {base.upper()}:",
                description=f"```{table}```",
                colour=0xEC6761,
            )
        )

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command()
    async def rate(self, ctx, base: str, *args):
        if base.upper() not in ABBREVIATIONS:
            return await ctx.send(
                f"The base you provided was invalid. To view valid currencies you can use `{ctx.prefix}currencies`\nUse `{ctx.prefix}help rate` to see argument format"
            )
        if not args:
            return await ctx.send(
                f"You did not provide any currencies to search for. To view valid currencies run `{ctx.prefix}currencies`\nUse `{ctx.prefix}help rate` to see argument format"
            )
        for arg in args:
            if arg.upper() not in ABBREVIATIONS:
                return await ctx.send(
                    f"One or more of the provided currencies was invalid. To view valid currencies run `{ctx.prefix}currencies`\nUse `{ctx.prefix}help rate` to see argument format"
                )

        async with aiohttp.ClientSession() as session:
            async with session.get(
                f"{self.base_url}latest?base={base.upper()}&symbols={','.join(args)}"
            ) as resp:
                resp.raise_for_status()
                data = await resp.json()

        table = PrettyTable(["Currency", "Rate"])
        for rate in data["rates"]:
            table.add_row([rate, round(data["rates"][rate], 3)])
        await ctx.send(
            embed=discord.Embed(
                title=f"Requested exchange rates from base {base}:",
                description=f"```{table}```",
                colour=0xEC6761,
            )
        )

    @commands.guild_only()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(hidden=True)
    async def ratehist(self, ctx, year: str, month: str, day: str, base: str = "USD"):
        if base.upper() not in ABBREVIATIONS:
            return await ctx.send(
                f"The base you provided was invalid. To view valid currencies you can use `{ctx.prefix}currencies`\nUse `{ctx.prefix}help rate` to see argument format"
            )
        if year is None or month is None or day is None:
            return await ctx.send(
                "Please specify a date to search. Format: `{ctx.prefix}ratehist YYYY MM DD`"
            )
        elif len(year) != 4 or len(month) != 2 or len(day) != 2:
            return await ctx.send(
                f"Some or all of the date values you entered were incorrect.\nThe correct format is `{ctx.prefix}ratehist YYYY MM DD`"
            )
        else:
            date_formatted = f"{year}-{month}-{day}"
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    f"{base_url}{date_formatted}?base={base.upper()}"
                ) as resp:
                    resp.raise_for_status()
                    data = await resp.json()
            table = PrettyTable(["Currency", "Rate"])
            try:
                for rate in data["rates"]:
                    table.add_row([rate, round(data["rates"][rate], 3)])
                return await ctx.send(
                    embed=discord.Embed(
                        title="Historical exchange rates for {date_formatted}:",
                        description=f"```{table}```",
                        colour=0xEC6761,
                    )
                )
            except KeyError:
                return await ctx.send(
                    "An API error occurred. Did you enter the correct values?"
                )

    @commands.guild_only()
    @commands.command()
    async def currencies(self, ctx):
        table = PrettyTable(["Abbreviation", "Currency"])
        for i in range(len(CURRENCIES)):
            table.add_row([ABBREVIATIONS[i], CURRENCIES[i]])
        await ctx.send(
            embed=discord.Embed(
                title="All valid currencies:",
                description=f"```{table}```",
                colour=0xEC6761,
            )
        )


def setup(bot):
    bot.add_cog(ExchangeRates(bot))
