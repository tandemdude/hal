import random
import json
import discord
from discord.ext import commands

pictures = {
    0: "```\n|\n|\n|\n|\n|\n|\n|_________```",
    1: "```\n__________\n|\n|\n|\n|\n|\n|\n|_________```",
    2: "```\n__________\n|        |\n|\n|\n|\n|\n|\n|_________```",
    3: "```\n__________\n|        |\n|        O\n|\n|\n|\n|\n|_________```",
    4: "```\n__________\n|        |\n|        O\n|        |\n|        |\n|\n|\n|_________```",
    5: "```\n__________\n|        |\n|        O\n|       /|\n|        |\n|\n|\n|_________```",
    6: "```\n__________\n|        |\n|        O\n|       /|\\\n|        |\n|\n|\n|_________```",
    7: "```\n__________\n|        |\n|        O\n|       /|\\\n|        |\n|       /\n|\n|_________```",
    8: "```\n__________\n|        |\n|        O\n|       /|\\\n|        |\n|       / \\\n|\n|_________```",
}


class Hangman(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        with open("hangman_words.json") as file:
            words = json.load(file)
        self.possible_words = words["words"]

    @commands.guild_only()
    @commands.group(invoke_without_command=True, aliases=["hm"])
    async def hangman(self, ctx):
        await ctx.send(
            "Available subcommands:\n```start - play hangman with a random word\nMore Coming Soon```"
        )

    @commands.guild_only()
    @commands.cooldown(1, 600, type=commands.BucketType.user)
    @hangman.command()
    async def start(self, ctx):
        """
        Start a hangman game with a random word from words.json
        Note: if bot does not have manage_messages permissions then this will get messy.
        This command has a 10 minute cooldown to prevent spam.
        """

        def create_embed(title, description, footer):
            emb = discord.Embed(title=title, description=description, colour=0xFF0000)
            emb.set_footer(text=footer)
            return emb

        def check(m):
            return (
                m.author == ctx.author
                and m.channel == ctx.channel
                and len(m.content) == 1
                and m.content.isalpha()
            )

        def full_space(word, previous_guesses):
            answer = ""
            for letter in word:
                if letter in previous_guesses:
                    answer += letter
                else:
                    answer += "\_ "

            return answer

        random_word = random.choice(self.possible_words)
        spaces = "\_ " * len(random_word)
        hangman = 0
        finished = False
        previous_guesses = []
        hangman_msg = await ctx.send(embed=create_embed(spaces, pictures[hangman], ""))

        while not finished:
            prompt = await ctx.send("Guess a letter:")

            try:
                guess = await self.bot.wait_for("message", check=check, timeout=30)
            except asyncio.TimeoutError:
                await prompt.edit(content="**Timed Out.**")
                return

            try:
                await prompt.delete()
                await guess.delete()
            except:
                pass
            if guess.content in previous_guesses:
                await ctx.send("You already guessed that letter", delete_after=5)
                continue
            previous_guesses.append(guess.content.lower())
            if guess.content.lower() in random_word:
                spaces = full_space(random_word, previous_guesses)
                if spaces.strip() == random_word:
                    finished = True
                    await hangman_msg.edit(
                        embed=create_embed(spaces, pictures[hangman], "You win! :)")
                    )
                else:
                    string = ", ".join(previous_guesses)
                    await hangman_msg.edit(
                        embed=create_embed(spaces, pictures[hangman], string)
                    )
            else:
                hangman += 1
                if hangman == 8:
                    await hangman_msg.edit(
                        embed=create_embed(
                            f"The word was: {random_word}",
                            pictures[hangman],
                            "You lose! :(",
                        )
                    )
                    finished = True
                else:
                    string = ", ".join(previous_guesses)
                    await hangman_msg.edit(
                        embed=create_embed(spaces, pictures[hangman], string)
                    )


def setup(bot):
    bot.add_cog(Hangman(bot))
