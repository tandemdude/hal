import discord
from discord.ext import commands
import time
import datetime
import psutil
import git


class Info(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command(aliases=["si", "gi", "guildinfo"])
    async def serverinfo(self, ctx):
        """Get general information about the current server/guild"""
        guild = ctx.guild
        bots = 0
        for member in guild.members:
            if member.bot:
                bots += 1

        emb = discord.Embed(title="Server Info:", colour=0xEC6761)
        emb.set_thumbnail(url=guild.icon_url)
        emb.add_field(name="Name:", value=guild.name, inline=True)
        emb.add_field(name="Owner:", value=str(guild.owner), inline=True)
        emb.add_field(
            name="Member Count:",
            value=f"Users: {len(guild.members) - bots}\nBots: {bots}",
            inline=True,
        )
        emb.add_field(
            name="Channel Count:",
            value=f"Text: {len(guild.text_channels)}\nVoice: {len(guild.voice_channels)}",
            inline=True,
        )
        emb.add_field(name="Role Count:", value=len(guild.roles))
        emb.add_field(name="Region:", value=guild.region, inline=True)
        emb.set_footer(text=f"ID: {guild.id}")
        await self.bot.send_embed(ctx, emb)

    @commands.guild_only()
    @commands.command(aliases=["mi"])
    async def memberinfo(self, ctx, *, member: discord.Member = None):
        """Get general information about yourself or other member if specified"""
        member = member if member is not None else ctx.author
        emb = discord.Embed(title=f"Info for user {str(member)}", colour=0xEC6761)
        emb.set_thumbnail(url=member.avatar_url)
        emb.add_field(name="Bot:", value=member.bot)
        emb.add_field(name="Status:", value=member.status)
        emb.add_field(name="Nickname:", value=member.display_name)
        emb.add_field(name="Highest Role:", value=member.top_role.name)
        emb.add_field(
            name="Created:", value=member.created_at.strftime("%Y-%m-%d %H:%M:%S UTC")
        )
        emb.add_field(
            name="Joined:", value=member.joined_at.strftime("%Y-%m-%d %H:%M:%S UTC")
        )
        emb.set_footer(text=f"ID: {member.id}")
        await self.bot.send_embed(ctx, emb)

    @commands.guild_only()
    @commands.command(aliases=["ri"])
    async def roleinfo(self, ctx, *, role: discord.Role = None):
        """Get general information about a specific role"""
        if role is None:
            return await ctx.send("No role specified.")
        else:
            emb = discord.Embed(
                title="Role Info",
                description=f"Shared by `{len(role.members)}` users",
                colour=role.colour.value,
            )
            emb.add_field(name="Name:", value=f"{role.name}", inline=True)
            emb.add_field(name="Hoisted:", value=f"{role.hoist}", inline=True)
            emb.add_field(name="Position:", value=f"{role.position}", inline=True)
            emb.add_field(name="Mentionable:", value=f"{role.mentionable}", inline=True)
            emb.add_field(name="Colour:", value=f"{str(role.colour)}", inline=True)
            emb.add_field(
                name="Created:",
                value=role.created_at.strftime("%Y-%m-%d %H:%M:%S UTC"),
                inline=True,
            )
            emb.set_footer(text=f"ID: {role.id}")
            await self.bot.send_embed(ctx, emb)

    @commands.guild_only()
    @commands.command()
    async def info(self, ctx):
        """Views general bot info"""
        commit = git.Repo().head.commit
        embed = discord.Embed(
            title="HAL Information",
            description=f"**Latest Commit** - {commit.authored_datetime.strftime('%Y-%m-%d')}\n`{commit.summary}`\n**By** - {commit.author.name}",
            colour=0xEC6761,
        )
        embed.set_thumbnail(url=self.bot.user.avatar_url)
        embed.set_footer(text=f"Total Commands Available: {len(self.bot.all_commands)}")
        embed.add_field(name="Version:", value="1.6.7", inline=True)
        embed.add_field(
            name="Support Server:",
            value="[link](https://discordapp.com/invite/yRc2Yfy)",
            inline=True,
        )
        embed.add_field(
            name="Library:",
            value=f"[discord.py](https://discordpy.readthedocs.io/en/latest/)\nVersion: {discord.__version__}",
            inline=True,
        )
        embed.add_field(
            name="Stats:",
            value=f"Guilds: {len(self.bot.guilds)}\nUsers: {len([*self.bot.get_all_members()])}",
            inline=True,
        )
        embed.add_field(
            name="Invite:",
            value="[link](https://discordapp.com/api/oauth2/authorize?client_id=606276772295868416&permissions=470117446&scope=bot)",
            inline=True,
        )
        embed.add_field(
            name="Website:", value="[link](https://hal-bot.xyz)", inline=True
        )
        embed.add_field(
            name="Memory:",
            value=f"{psutil.virtual_memory().percent}% load",
            inline=True,
        )
        embed.set_image(
            url="https://top.gg/api/widget/owner/606276772295868416.png"
        )
        await ctx.send(embed=embed)

    @commands.guild_only()
    @commands.command(aliases=["sys", "sysinfo"])
    async def system(self, ctx):
        """Views general system info"""
        cpu_usage = psutil.cpu_percent(percpu=True)
        ram_usage = psutil.virtual_memory().percent
        swap_usage = psutil.swap_memory().percent
        embed = discord.Embed(
            title="System Information",
            description=f"```CPU 0: {cpu_usage[0]}%\nCPU 1: {cpu_usage[1]}%\nCPU 2: {cpu_usage[2]}%\nCPU 3: {cpu_usage[3]}%\n===============\nRAM: {ram_usage}%\nSWAP: {swap_usage}%```",
            colour=0xEC6761,
        )
        embed.set_thumbnail(
            url="https://www.raspberrypi.org/wp-content/uploads/2011/10/Raspi-PGB001.png"
        )
        await self.bot.send_embed(ctx, embed)

    @commands.guild_only()
    @commands.command()
    async def vote(self, ctx):
        """View bot vote link(s)"""
        links = [
            "[discordbots.org/top.gg](https://top.gg/bot/606276772295868416/vote)",
            "[bots.ondiscord.xyz](https://bots.ondiscord.xyz/bots/606276772295868416)",
            "[botsfordiscord.com](https://botsfordiscord.com/bot/606276772295868416/vote)",
            "[discordbotlist.com](https://discordbotlist.com/bots/606276772295868416/upvote)",
            "[botlist.space](https://botlist.space/bot/606276772295868416/upvote)",
            "[discord.boats](https://discord.boats/bot/606276772295868416/vote)"
        ]
        await ctx.send(
            embed=discord.Embed(
                title="Vote links:", description="\n".join(links), colour=0xEC6761
            )
            .set_thumbnail(url=self.bot.user.avatar_url)
            .set_footer(text="Get 250 coins for voting with the first link.")
        )

    @commands.guild_only()
    @commands.command()
    async def news(self, ctx):
        news_channel = self.bot.get_channel(606285199302459405)
        hist = await news_channel.history(limit=1).flatten()
        newest_message = hist[0]
        await ctx.send(
            embed=discord.Embed(
                title="HAL - Latest News:",
                description=newest_message.content,
                colour=0xEC6761,
            ).set_footer(
                text=f"Posted by {str(newest_message.author)}",
                icon_url=newest_message.author.avatar_url,
            )
        )


def setup(bot):
    bot.add_cog(Info(bot))
