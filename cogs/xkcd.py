import discord
from discord.ext import commands
import aiohttp
import random

FORBIDDEN_COMICS = [631, 404]


class Xkcd(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.group(invoke_without_command=True)
    async def xkcd(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```current - get the current comic\nnum <number> - get comic with specified number\nrandom - get a random comic```"
        )

    @commands.guild_only()
    @xkcd.command()
    async def current(self, ctx):
        url = "http://xkcd.com/info.0.json"
        async with aiohttp.request("get", url) as resp:
            data = await resp.json()
            embed = discord.Embed(
                title=f"Number {data['num']}: {data['title']}",
                description=data["alt"],
                colour=0xFF6961,
            )
            embed.set_image(url=data["img"])
            embed.set_footer(
                text=f"Dated: {data['year']}-{data['month']}-{data['day']}"
            )
            await ctx.send(embed=embed)

    @commands.guild_only()
    @xkcd.command(aliases=["number", "no"])
    async def num(self, ctx, num: int = None):
        if num is None:
            return await ctx.send(
                "Please include the number of the comic you wish to view."
            )
        elif num in FORBIDDEN_COMICS:
            return await ctx.send(
                "That comic has been forbidden from appearing while using this command."
            )

        url = f"http://xkcd.com/{num}/info.0.json"
        async with aiohttp.request("get", url) as resp:
            data = await resp.json()
            embed = discord.Embed(
                title=f"Number {data['num']}: {data['title']}",
                description=data["alt"],
                colour=0xFF6961,
            )
            embed.set_image(url=data["img"])
            embed.set_footer(
                text=f"Dated: {data['year']}-{data['month']}-{data['day']}"
            )
            await ctx.send(embed=embed)

    @commands.guild_only()
    @xkcd.command(aliases=["rand", "ran"])
    async def random(self, ctx):
        async with aiohttp.ClientSession() as session:
            resp = await session.get("http://xkcd.com/info.0.json")
            data = await resp.json()
            maxnum = data["num"]
            random_num = random.randint(1, int(maxnum))

            if random_num in FORBIDDEN_COMICS:
                random_num -= 1

            resp = await session.get(f"http://xkcd.com/{random_num}/info.0.json")
            data = await resp.json()
            embed = discord.Embed(
                title=f"Number {data['num']}: {data['title']}",
                description=data["alt"],
                colour=0xFF6961,
            )
            embed.set_image(url=data["img"])
            embed.set_footer(
                text=f"Dated: {data['year']}-{data['month']}-{data['day']}"
            )
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Xkcd(bot))
