from discord.ext import commands
import discord
import aiohttp
import random
import datetime
import aiosqlite
import hashlib

possible_responses = [
    "It is certain",
    "It is decidely so",
    "Without a doubt",
    "Yes - definitely",
    "you may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",
    "Reply hazy, try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful",
]


class Misc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command(name="8ball")
    async def eightball(self, ctx):
        await ctx.send(f"🎱 {random.choice(possible_responses)}")

    @commands.guild_only()
    @commands.command()
    async def rps(self, ctx, choice=None):
        """Play rock, paper, scissors against the bot"""
        rps_options = ["rock", "paper", "scissors"]
        if choice != "rock" and choice != "paper" and choice != "scissors":
            await ctx.send(
                "You need to specify a choice! Options are: `rock`, `paper` or `scissors`."
            )
        else:
            bot_choice = random.choice(rps_options)
            if (
                (choice == "rock" and bot_choice == "rock")
                or (choice == "paper" and bot_choice == "paper")
                or (choice == "scissors" and bot_choice == "scissors")
            ):
                await ctx.send(f"Draw! We both picked {choice}")
            elif (
                (choice == "rock" and bot_choice == "scissors")
                or (choice == "paper" and bot_choice == "rock")
                or (choice == "scissors" and bot_choice == "paper")
            ):
                await ctx.send(f"You win! I picked {bot_choice}")
            elif (
                (choice == "rock" and bot_choice == "paper")
                or (choice == "paper" and bot_choice == "scissors")
                or (choice == "scissors" and bot_choice == "rock")
            ):
                await ctx.send(f"I win! My choice was {bot_choice}")
            else:
                await ctx.send(
                    ":x: Oh No! You found an error. Please dm my creator, thomm.o#0001, with details. :x:"
                )

    @commands.guild_only()
    @commands.command(aliases=["flip"])
    async def toss(self, ctx):
        """Flip a fair, two sided coin"""
        coin_options = ["Heads", "Tails"]
        await ctx.send(f"You flipped a coin. It landed {random.choice(coin_options)}")

    @commands.guild_only()
    @commands.command()
    async def len(self, ctx, *, arg):
        """Get length of any input string"""
        await ctx.send(f"Your text is `{len(arg)}` characters long!")

    @commands.guild_only()
    @commands.command()
    async def mock(self, ctx, *, arg):
        """Mock a message liKe tHiS"""
        await ctx.send(
            "".join(
                [
                    arg[x].upper() if random.randint(0, 1) else arg[x].lower()
                    for x in range(len(arg))
                ]
            )
        )

    @commands.guild_only()
    @commands.command()
    async def avatar(self, ctx, user: discord.User = None):
        """Get big avatar of yourself or a specified user"""
        if user is not None:
            await ctx.send(
                embed=discord.Embed(title=f"{user.name}'s Avatar:").set_image(
                    url=user.avatar_url_as(static_format="png")
                )
            )
        else:
            await ctx.send(
                embed=discord.Embed(title=f"{ctx.author.name}'s Avatar:").set_image(
                    url=ctx.author.avatar_url_as(static_format="png")
                )
            )

    @commands.guild_only()
    @commands.command()
    async def say(self, ctx, *, message):
        await ctx.send(
            embed=discord.Embed(
                title=None,
                description=message,
                colour=0xEC6761,
                timestamp=datetime.datetime.utcnow(),
            )
        )

    @commands.guild_only()
    @commands.command()
    async def invite(self, ctx):
        await ctx.send(
            embed=discord.Embed(
                title="Invite Link",
                url="https://discordapp.com/api/oauth2/authorize?client_id=606276772295868416&permissions=470117446&scope=bot",
                description="Add me to your own\nserver by clicking\nthe title above",
                colour=0xEC6761,
            ).set_thumbnail(url=self.bot.user.avatar_url)
        )

    @commands.guild_only()
    @commands.command()
    async def cmds(self, ctx, user: discord.User = None):
        user = user if user is not None else ctx.author

        async with aiosqlite.connect(self.bot.db_file) as db:
            async with db.execute(
                f"SELECT commands_run FROM users WHERE(user_id = {user.id});"
            ) as cursor:
                row = await cursor.fetchone()

                commands_run = row[0] if row is not None else 1

                await ctx.send(
                    embed=discord.Embed(
                        title=f"Total commands run",
                        description=f"All-Time commands run: `{commands_run}`",
                        colour=0xEC6761,
                    ).set_footer(
                        text=f"Stats for {str(user)}", icon_url=user.avatar_url
                    )
                )

    @commands.guild_only()
    @commands.command()
    async def thot(self, ctx, user: discord.User = None):
        user = user if user is not None else ctx.author
        percent_thot = user.id * 2 % 100
        await ctx.send(
            embed=discord.Embed(
                title="Result:",
                description=f"{user.mention} is {percent_thot}% thot <:thot:614223310732918787>",
                colour=0xEC6761,
            )
        )

    @commands.guild_only()
    @commands.command()
    async def gay(self, ctx, user: discord.User = None):
        user = user if user is not None else ctx.author
        percent_gay = user.id % 100
        await ctx.send(
            embed=discord.Embed(
                title="Result:",
                description=f"{user.mention} is {percent_gay}% gay :gay_pride_flag:",
                colour=0xEC6761,
            )
        )

    @commands.guild_only()
    @commands.command()
    async def tips(self, ctx, option: str):
        possible_responses = {"on": 1, "off": 0}
        if option.lower() in possible_responses.keys():
            async with aiosqlite.connect(self.bot.db_file) as db:
                async with db.execute("SELECT tips FROM users WHERE(user_id = ?);", [ctx.author.id]) as cursor:
                    row = await cursor.fetchone()
                    if row is None:
                        return await ctx.send("An internal error occured, please contact the dev if this persists")
                    else:
                        await db.execute("UPDATE users SET tips = ? WHERE user_id = ?", [possible_responses[option.lower()], ctx.author.id])
                        await db.commit()
                        return await ctx.send("Preferences changed successfully.")
        else:
            return await ctx.send("Invalid option. Usage:\n`h.tips on` to enable tips\n`h.tips off` to disable tips")


def setup(bot):
    bot.add_cog(Misc(bot))
