import discord
from discord.ext import commands
import os


class Analysis(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.guild_only()
    @commands.command()
    async def codeanalysis(self, ctx):
        await ctx.trigger_typing()
        e = discord.Embed(title="Code stats", colour=0xEC6761)

        files = ["bot.py", "custombot.py"]
        for file in os.listdir("cogs"):
            files.append(f"cogs/{file}")

        total_lines = 0
        total_py_files = 0
        comments = 0
        awaits = 0
        asyncs = 0
        imports = 0
        embeds = 0
        for file in files:
            if file.endswith(".py"):
                total_py_files += 1
                total_lines += int(os.popen(f"wc -l < {file}").read())
                comments += int(os.popen(f'grep -c "#" {file}').read())
                awaits += int(os.popen(f'grep -c "await" {file}').read())
                asyncs += int(os.popen(f'grep -c "async" {file}').read())
                imports += int(os.popen(f'grep -c "import" {file}').read())
                embeds += int(os.popen(f'grep -c "discord.Embed" {file}').read())

        e.add_field(name="Extensions:", value=total_py_files)
        e.add_field(name="Lines of code:", value=total_lines)
        e.description = f"Comments: {comments}\nUses of `await` keyword: {awaits}\nUses of `async` keyword: {asyncs}\nImports: {imports}\nEmbeds: {embeds}"
        e.set_thumbnail(
            url="https://cdn3.iconfinder.com/data/icons/meanicons-base/512/meanicons_57-512.png"
        )
        await self.bot.send_embed(ctx, e)


def setup(bot):
    bot.add_cog(Analysis(bot))
