import discord
from discord.ext import commands
import aiosqlite
import datetime


class Logging(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.db_file = "botdata.db"

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @commands.group(invoke_without_command=True)
    async def logging(self, ctx):
        await ctx.send(
            "Available Subcommands:\n```setchannel #channel - set the channel for logging messages to be posted in\ndisable - disables logging messages for the server```"
        )

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @logging.command()
    async def setchannel(self, ctx, channel: discord.TextChannel = None):
        """Set channel for logging messages to be sent to"""
        if channel is None:
            return await ctx.send(
                "You need to specify a channel for logging messages to appear in."
            )
        else:
            logging_channel_id = channel.id

            async with aiosqlite.connect(self.db_file) as db:
                async with db.execute(
                    f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
                ) as cursor:
                    row = await cursor.fetchone()

                    if row is not None:
                        await db.execute(
                            f"UPDATE guilds SET logging_id = {logging_channel_id} WHERE guild_id = {ctx.guild.id}"
                        )
                        await db.commit()
                    else:
                        await ctx.send(
                            "Please wait, creating a record for you in the database."
                        )
                        await db.execute(
                            f"INSERT INTO guilds(guild_id, welcome_id, role_id, logging_id, suggestion_id, announcement_id) VALUES({ctx.guild.id}, NULL, NULL, {logging_channel_id}, NULL, NULL);"
                        )
                        await db.commit()
            return await ctx.send("Logging channel registered.")

    @commands.guild_only()
    @commands.has_permissions(manage_channels=True)
    @logging.command()
    async def disable(self, ctx):
        """Disable logging messages in the guild"""
        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT * FROM guilds WHERE(guild_id = {ctx.guild.id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is None:
                    return await ctx.send(
                        "No record found. Contact my creator if you believe this to be incorrect."
                    )
                else:
                    await ctx.send("Disabling logging...")
                    await db.execute(
                        f"UPDATE guilds SET logging_id = NULL WHERE guild_id = {ctx.guild.id}"
                    )
                    await db.commit()
                    return await ctx.send("Logging disabled successfully.")

    @commands.Cog.listener()
    async def on_message_edit(self, before, after):
        if before.author.id == self.bot.user.id:
            return

        guild_id = before.guild.id

        embed = discord.Embed(
            title=f"{str(before.author)} edited a message in #{before.channel.name}",
            colour=0xFFB347,
            timestamp=datetime.datetime.utcnow(),
        )
        embed.add_field(name="Before:", value=before.content, inline=False)
        embed.add_field(name="After:", value=after.content, inline=False)

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT logging_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    logging_channel_id = row[0]
                    try:
                        logging_channel = before.guild.get_channel(logging_channel_id)
                        await logging_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass

    @commands.Cog.listener()
    async def on_message_delete(self, message):
        if message.author.id == self.bot.user.id:
            return

        guild_id = message.guild.id

        embed = discord.Embed(
            title=f"A message from was deleted in #{message.channel.name}",
            colour=0xFF6961,
            timestamp=datetime.datetime.utcnow(),
        )
        embed.add_field(name=f"Sent by {str(message.author)}", value=message.content)

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT logging_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    logging_channel_id = row[0]
                    try:
                        logging_channel = message.guild.get_channel(logging_channel_id)
                        await logging_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass

    @commands.Cog.listener()
    async def on_member_ban(self, guild, user):
        guild_id = guild.id

        embed = discord.Embed(
            title=f"{str(user)} was banned from the server.",
            colour=0xFF6961,
            timestamp=datetime.datetime.utcnow(),
        )

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT logging_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    logging_channel_id = row[0]
                    try:
                        logging_channel = guild.get_channel(logging_channel_id)
                        await logging_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass

    @commands.Cog.listener()
    async def on_member_unban(self, guild, user):
        guild_id = guild.id

        embed = discord.Embed(
            title=f"{str(user)} was unbanned from the server.",
            colour=0x77DD77,
            timestamp=datetime.datetime.utcnow(),
        )

        async with aiosqlite.connect(self.db_file) as db:
            async with db.execute(
                f"SELECT logging_id FROM guilds WHERE(guild_id = {guild_id});"
            ) as cursor:
                row = await cursor.fetchone()

                if row is not None:
                    logging_channel_id = row[0]
                    try:
                        logging_channel = guild.get_channel(logging_channel_id)
                        await logging_channel.send(embed=embed)
                    except (discord.Forbidden, AttributeError):
                        pass


def setup(bot):
    bot.add_cog(Logging(bot))
