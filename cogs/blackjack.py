import discord
from discord.ext import commands
import aiohttp
import asyncio

CARD_DATA = {
    "AH": {"emoji": "<:AH:610482647822762015>", "value": 11},
    "2H": {"emoji": "<:2H:610482611839696937>", "value": 2},
    "3H": {"emoji": "<:3H:610482649387106304>", "value": 3},
    "4H": {"emoji": "<:4H:610482653321363466>", "value": 4},
    "5H": {"emoji": "<:5H:610482653912760360>", "value": 5},
    "6H": {"emoji": "<:6H:610482655552733195>", "value": 6},
    "7H": {"emoji": "<:7H:610482656546783272>", "value": 7},
    "8H": {"emoji": "<:8H:610482658589409291>", "value": 8},
    "9H": {"emoji": "<:9H:610482658534883331>", "value": 9},
    "0H": {"emoji": "<:0H:610482589894967347>", "value": 10},
    "JH": {"emoji": "<:JH:610482693712510976>", "value": 10},
    "QH": {"emoji": "<:QH:610482786645573672>", "value": 10},
    "KH": {"emoji": "<:KH:610482693909774337>", "value": 10},
    "AC": {"emoji": "<:AC:610482647696801904>", "value": 11},
    "2C": {"emoji": "<:2C:610482610191466506>", "value": 2},
    "3C": {"emoji": "<:3C:610482612661780510>", "value": 3},
    "4C": {"emoji": "<:4C:610482654206361651>", "value": 4},
    "5C": {"emoji": "<:5C:610482655343018005>", "value": 5},
    "6C": {"emoji": "<:6C:610482657335312395>", "value": 6},
    "7C": {"emoji": "<:7C:610482657276592148>", "value": 7},
    "8C": {"emoji": "<:8C:610482658656518145>", "value": 8},
    "9C": {"emoji": "<:9C:610482659575201792>", "value": 9},
    "0C": {"emoji": "<:0C:610482578889375776>", "value": 10},
    "JC": {"emoji": "<:JC:610482693171576892>", "value": 10},
    "QC": {"emoji": "<:QC:610482693662048276>", "value": 10},
    "KC": {"emoji": "<:KC:610482692848484392>", "value": 10},
    "AD": {"emoji": "<:AD:610482647856054283>", "value": 11},
    "2D": {"emoji": "<:2D:610482611353157634>", "value": 2},
    "3D": {"emoji": "<:3D:610482613379006472>", "value": 3},
    "4D": {"emoji": "<:4D:610482649466929152>", "value": 4},
    "5D": {"emoji": "<:5D:610482653304455168>", "value": 5},
    "6D": {"emoji": "<:6D:610482655380766742>", "value": 6},
    "7D": {"emoji": "<:7D:610482656785727488>", "value": 7},
    "8D": {"emoji": "<:8D:610482657364803606>", "value": 8},
    "9D": {"emoji": "<:9D:610482658530689024>", "value": 9},
    "0D": {"emoji": "<:0D:610482585243746326>", "value": 10},
    "JD": {"emoji": "<:JD:610482693611978752>", "value": 10},
    "QD": {"emoji": "<:QD:610482693297405952>", "value": 10},
    "KD": {"emoji": "<:KD:610482693750128640>", "value": 10},
    "AS": {"emoji": "<:AS:610482647935746080>", "value": 11},
    "2S": {"emoji": "<:2S:610482613269823509>", "value": 2},
    "3S": {"emoji": "<:3S:610482652927098948>", "value": 3},
    "4S": {"emoji": "<:4S:610482653338271774>", "value": 4},
    "5S": {"emoji": "<:5S:610482655414321153>", "value": 5},
    "6S": {"emoji": "<:6S:610482657826045952>", "value": 6},
    "7S": {"emoji": "<:7S:610482657251426336>", "value": 7},
    "8S": {"emoji": "<:8S:610482658383757314>", "value": 8},
    "9S": {"emoji": "<:9S:610482659033874442>", "value": 9},
    "0S": {"emoji": "<:0S:610482595305619466>", "value": 10},
    "JS": {"emoji": "<:JS:610482693414584320>", "value": 10},
    "QS": {"emoji": "<:QS:610482787174187008>", "value": 10},
    "KS": {"emoji": "<:KS:610482693309726750>", "value": 10},
}


class Blackjack(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.new_deck_url = (
            "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6"
        )

    @commands.guild_only()
    @commands.is_owner()
    @commands.command(hidden=True)
    async def blackjack(self, ctx, bet: int = 0):
        def check(r, u):
            return str(r.emoji) in ["🇭", "🇸"] and u.id == ctx.author.id

        running = True

        player_cards = []
        player_value = 0
        bot_cards = []
        bot_value = 0

        async def get_new_deck():
            async with aiohttp.ClientSession() as session:
                async with session.get(self.new_deck_url) as resp:
                    resp.raise_for_status()
                    data = await resp.json()
                    return data["deck_id"]

        async def draw_card(deck_id):
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    f"https://deckofcardsapi.com/api/deck/{deck_id}/draw/?count=1"
                ) as resp:
                    resp.raise_for_status()
                    data = await resp.json()
                    return data["cards"][0]["code"]

        async def process_reaction(reaction, emb, cards, deck_id):
            emj = str(reaction)
            if emj == "🇭":
                cards.append(await draw_card(deck_id))
                card_emojis = []
                for card in cards:
                    card_emojis.append(CARD_DATA[card]["emoji"])
                emb.remove_field(1)
                emb.add_field(name="Your cards:", value="".join(card_emojis))
                return emb
            elif emj == "🇸":
                # Bot draws cards
                pass

        def check_totals(pcards, bcards, ace_val: int=11):
            pass

        def process_game_over(pcards, bcards, player_won):
            emb = discord.Embed(
                title=f"{ctx.author.name}'s blackjack game",
                description=f"Playing for `{bet}` coins\nBot will always stand on 17.",
                colour=0xEC6761,
            )
            emjs = []
            for card in bcards:
                emjs.append(CARD_DATA[card]["emoji"])
            emb.add_field(name="Bot's cards:", value="".join(emjs))
            emjs = []
            for card in pcards:
                emjs.append(CARD_DATA[card]["emoji"])
            emb.add_field(name="Your cards:", value="".join(emjs))

            if player_won:
                emb.set_footer(text="You win!")
                # Add Money
            else:
                emb.set_footer(text="You lose!")
                # Remove Money

        m = await ctx.send("Shuffling the deck <a:loading:599345321084190760>")
        deck_id = await get_new_deck()
        await m.edit(content="Deck Shuffled, starting game...")

        # Draw 2 cards for both the player and bot
        player_cards.append(await draw_card(deck_id))
        bot_cards.append(await draw_card(deck_id))
        player_cards.append(await draw_card(deck_id))
        bot_cards.append(await draw_card(deck_id))

        game_embed = discord.Embed(
            title=f"{ctx.author.name}'s blackjack game",
            description=f"Playing for `{bet}` coins\nBot will always stand on 17.",
            colour=0xEC6761,
        )
        game_embed.add_field(name="Bot's cards:", value=f"{CARD_DATA[bot_cards[0]]['emoji']}🎴")
        game_embed.add_field(
            name="Your cards:",
            value=f"{CARD_DATA[player_cards[0]]['emoji']}{CARD_DATA[player_cards[1]]['emoji']}",
        )
        game_embed.set_footer(text="[H] - Hit, [S] - Stand")

        game_msg = await ctx.send(embed=game_embed)
        await game_msg.add_reaction("🇭")
        await game_msg.add_reaction("🇸")

        player_turn = True
        bot_turn = False

        while running:
            while player_turn:
                try:
                    reaction, user = await self.bot.wait_for(
                        "reaction_add", check=check, timeout=20
                    )
                    game_embed = await process_reaction(
                        reaction, game_embed, player_cards, deck_id
                    )
                    check_totals(player_cards, bot_cards)
                    await game_msg.edit(embed=game_embed)
                except asyncio.TimeoutError:
                    game_embed.description = (
                        "**Timed Out**\nBot will always stand on 17."
                    )
                    await game_msg.edit(embed=game_embed)
                    return


def setup(bot):
    bot.add_cog(Blackjack(bot))
