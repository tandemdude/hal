# Coded by github u/tandemdude
# https://github.com/tandemdude
import discord
from discord.ext import commands
import time
import configparser
import logging
import custombot

logging.basicConfig(level=logging.INFO)

# Sets up parser and reads the file containing the bot token
parser = configparser.ConfigParser()
parser.read("TOKEN.INI")

# List of all extensions to be loaded
extensions = [
    "cogs.announcements",
    "cogs.autorole",
    "cogs.bigemoji",
    "cogs.codeanalysis",
    "cogs.connectfour",
    "cogs.currency",
    "cogs.dblapi",
    "cogs.exchangerates",
    "cogs.extensionmanager",
    "cogs.hangman",
    "cogs.imagemanip",
    "cogs.info",
    "cogs.jobs",
    "cogs.levelling",
    "cogs.listeners",
    "cogs.logging",
    "cogs.minecraft",
    "cogs.minesweeper",
    "cogs.misc",
    "cogs.moderation",
    "cogs.onready",
    "cogs.owner",
    "cogs.ownersuggestions",
    "cogs.pchardware",
    "cogs.pingpong",
    "cogs.purge",
    "cogs.statlogging",
    "cogs.suggestions",
    "cogs.uptime",
    "cogs.welcome",
    "cogs.xkcd",
    "libneko.extras.superuser",
    "libneko.extras.help"
]

# Declares the bot prefix and token, taking values from files
prefix = "h."
token = parser["DEFAULT"]["token"]


# Main function creates bot, loads extensions and runs the bot
def run_bot():
    bot = custombot.Bot(command_prefix=prefix, case_insensitive=True)
    if len(extensions) != 0:
        for ext in extensions:
            bot.load_extension(ext)
            print(f"Successfully loaded {ext}")
    bot.run(token)


# Keeps the bot alive
while True:
    run_bot()
    time.sleep(5)
